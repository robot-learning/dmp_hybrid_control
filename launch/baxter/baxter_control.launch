<?xml version="1.0"?>
<launch>

  <arg name="robot_name"                 default="baxter"/>
  <arg name="arm"                        default="right"/>
  <arg name="sim"                        default="false"/>
  <arg name="js_position"                default="false"/>
  <arg name="inv_dyn"                    default="false"/>
  <arg name="hybrid"                     default="false"/>
  <arg name="parallel"                   default="false"/>
  <arg name="force_null"                 default="false"/>
  <arg name="posture"                    default="true"/>
  <arg name="compensate_gravity"         default="true"/>
  <arg name="switch_control"             default="false"/>
  <arg name="constraint_frame"           default="false"/>
  <arg name="run_rate"                   default="500"/>
  <arg name="spring_compensation_factor" default="1.0"/>
  <arg name="root_link"                  default="$(arg arm)_arm_mount"/>
  <arg name="tip_link"                   default="push_ball_center"/>
  <arg name="jnt_state_topic"            default="/baxter/$(arg arm)_arm/joint_states"/>
  <arg name="sea_jnt_state_topic"        default="/robot/limb/$(arg arm)/gravity_compensation_torques"/>
  <arg name="disable_gravity_topic"      default="/robot/limb/$(arg arm)/suppress_gravity_compensation"/>
  <arg name="jnt_cmd_topic"              default="joint_cmd"/>
  <arg name="jnt_des_cmd_topic"          default="/baxter/jnt_des_cmd"/>
  <arg name="os_cmd_topic"               default="/baxter/os_cmd"/>
  
  <!-- ======================================================================================= -->

  <!-- Load control parameters -->
  <group if="$(arg js_position)">
    <rosparam file="$(find ll4ma_robot_control)/config/baxter_position_control.yaml"
              command="load"/>
    <group ns="$(arg robot_name)">
      <param name="control_type" type="str" value="js_position"/>
    </group>
  </group>
  <group if="$(arg inv_dyn)">
    <rosparam file="$(find ll4ma_robot_control)/config/baxter_inv_dyn_control.yaml"
              command="load"/>
    <group ns="$(arg robot_name)">
      <param name="control_type" type="str" value="os_inv_dyn"/>
    </group>
  </group>
  <group if="$(arg force_null)">
    <rosparam file="$(find ll4ma_robot_control)/config/baxter_force_null_inv_dyn.yaml"
              command="load"/>
    <group ns="$(arg robot_name)">
      <param name="control_type" type="str" value="os_force_null_inv_dyn"/>
    </group>
  </group>
  <group if="$(arg hybrid)">
    <rosparam file="$(find ll4ma_robot_control)/config/baxter_hybrid_control.yaml"
              command="load"/>
    <group ns="$(arg robot_name)">
      <param name="control_type" type="str" value="hybrid_force_position"/>
    </group>
  </group>
  
  <!-- load params into parameter server -->
  <group ns="$(arg robot_name)">
    <param name="posture"                    type="bool"   value="$(arg posture)"/>
    <param name="compensate_gravity"         type="bool"   value="$(arg compensate_gravity)"/>
    <param name="switch_control"             type="bool"   value="$(arg switch_control)"/>  
    <param name="use_constraint_frame"       type="bool"   value="$(arg constraint_frame)"/>
    <param name="run_rate"                   type="int"    value="$(arg run_rate)"/>
    <param name="root_link"                  type="str"    value="$(arg root_link)"/>
    <param name="tip_link"                   type="str"    value="$(arg tip_link)"/>
    <param name="jnt_state_topic"            type="str"    value="$(arg jnt_state_topic)"/>
    <param name="sea_jnt_state_topic"        type="str"    value="$(arg sea_jnt_state_topic)"/>
    <param name="disable_gravity_topic"      type="str"    value="$(arg disable_gravity_topic)"/>
    <param name="jnt_cmd_topic"              type="str"    value="$(arg jnt_cmd_topic)"/>
    <param name="jnt_des_cmd_topic"          type="str"    value="$(arg jnt_des_cmd_topic)"/>
    <param name="os_cmd_topic"               type="str"    value="$(arg os_cmd_topic)"/>
    <param name="spring_compensation_factor" type="double" value="$(arg spring_compensation_factor)"/>
  </group>

  <group unless="$(arg sim)">
    <rosparam file="$(find ll4ma_robot_control)/config/baxter_control.yaml" command="load"/>
  </group>
  
  <!-- Controller manager -->
  <node name="ll4ma_controller_manager" pkg="ll4ma_robot_control" type="controller_manager"
        args="$(arg robot_name)" respawn="false" output="screen"/>  

  <!-- ======================================= Simulation ===================================== -->

  <group if="$(arg sim)">
    <!-- load controller configurations from YAML file to parameter server -->
    <rosparam file="$(find ll4ma_robot_control)/config/baxter_sim_control.yaml" command="load"/>

    <!-- load all the controllers -->
    <node name="effort_controller_loader" pkg="controller_manager" type="spawner" respawn="false"
          output="screen" ns="/baxter" args="joint_state_controller
					     $(arg arm)_s0_effort_controller
					     $(arg arm)_s1_effort_controller
					     $(arg arm)_e0_effort_controller
					     $(arg arm)_e1_effort_controller
					     $(arg arm)_w0_effort_controller
					     $(arg arm)_w1_effort_controller
					     $(arg arm)_w2_effort_controller
					     "/>

    <!-- load joint wrapper to distribute joint command across individual controllers -->
    <node name="baxter_joint_wrapper" pkg="urlg_robots_gazebo" type="robot_joint_wrapper"
          respawn="false" output="screen">
      <param name="listen_prefix" value="/baxter"/>
      <param name="publish_prefix" value="/baxter"/>
      <param name="control_loop_rate" type="int" value="$(arg run_rate)"/>
      <param name="control_method" type="str" value="e"/>
    </node>

    <!-- switch control -->
    <node name="control_switcher" pkg="ll4ma_robot_control" type="baxter_move_to_position.py"
          respawn="false" output="screen"/>
  </group>
  
</launch>
