#!/usr/bin/env python
import os
import sys
import argparse
import rospy
import baxter_interface
from logger import H5Interface
from optoforce_etherdaq_driver.srv import ZeroForceSensor
from ll4ma_robot_control.msg import RobotState


if __name__ == '__main__':
    rospy.init_node('command_trajectory')
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', nargs='+', dest='phases', type=int)
    parser.add_argument('-i', dest='idx', type=int, default='-1')
    parser.add_argument('-d', '--dmp', dest='dmp', action='store_true')
    parser.add_argument('-f', dest='filename', type=str, default="command_trajectories.h5")
    args = parser.parse_args(sys.argv[1:])

    robot_state_received = False

    def robot_state_cb(robot_state):
        global robot_state_received
        robot_state_received = True

    rospy.Subscriber("/baxter/robot_state", RobotState, robot_state_cb)

    if args.idx < 0:
        # get latest trajectory
        path = os.path.expanduser("~/.ros/ll4ma_robot_control")
        index_file = args.filename.split('.')[0]
        with open (os.path.join(path, "." + index_file + ".index")) as f:
            idx = str(int(f.readline()) - 1)
    else:
        idx = str(args.idx)

    trajectory = 'trajectory_' + idx
    
    phases = []
    if args.phases is None:
        phases.append('full')
    else:
        for i in args.phases:
            phases.append("phase_%d" % i)
        
    r = baxter_interface.Limb('right')
    h = H5Interface(h5_filename=args.filename)
    rospy.loginfo("[TrajectoryCommander] Moving to initial trajectory position...")

    data = h.get_data(trajectory)
    start = data[phases[0]].attrs['start'] if phases[0] != 'full' else 0
    init_jnts = data['raw_data']['q'][:,start]
    jnt_names = r.joint_names()
    r.move_to_joint_positions(dict(zip(jnt_names, init_jnts)))
    # let it settle, then command again to make sure it's there
    rospy.sleep(2.0)
    r.move_to_joint_positions(dict(zip(jnt_names, init_jnts)))
    if args.dmp:
        rospy.loginfo("[TrajectoryCommander] DMP TRAJECTORY")
        rospy.set_param("dmp", "true")
    else:
        rospy.loginfo("[TrajectoryCommander] DIRECT PLAYBACK")
        rospy.set_param("dmp", "false")
    if not phases:
        rospy.loginfo("[TrajectoryCommander] Ready to command full TRAJECTORY %s" % trajectory)
    else:
        rospy.loginfo("[TrajectoryCommander] Ready to command these PHASES of TRAJECTORY %s in order:" % trajectory)
        for phase in phases:
            rospy.loginfo("[TrajectoryCommander]     %s" % phase)

    rospy.loginfo("[TrajectoryCommander] Waiting for controller to start...")

    rate = rospy.Rate(100)
    while not rospy.is_shutdown() and not robot_state_received:
        rate.sleep()

    if robot_state_received:
        rospy.loginfo("[TrajectoryCommander] Controller started!")
        rospy.loginfo("[TrajectoryCommander] Zeroing force sensor...")
        rospy.wait_for_service("zero_force_sensor")
        try:
            zero_force_sensor = rospy.ServiceProxy("zero_force_sensor", ZeroForceSensor)
            success = zero_force_sensor(True)
        except rospy.ServiceException, e:
            rospy.logerr("[TrajectoryCommander] Could not zero force sensor: %s" % e)
        if success:
            rospy.loginfo("[TrajectoryCommander] Forces zeroed!")
            h = H5Interface(h5_filename=args.filename, trajectory_client=True,
                            rospy_init=False, rospy_init_tc=False)
            rospy.loginfo("[TrajectoryCommander] Commanding: " + trajectory)
            rospy.loginfo("[TrajectoryCommander] 3")
            rospy.sleep(1.0)
            rospy.loginfo("[TrajectoryCommander] 2")
            rospy.sleep(1.0)
            rospy.loginfo("[TrajectoryCommander] 1")
            rospy.sleep(1.0)
            if args.dmp:
                h.command_dmp_trajectory(trajectory, phases)
            else:
                h.command_trajectory(trajectory, phases)    
