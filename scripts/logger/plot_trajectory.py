#!/usr/bin/env python
import os
import sys
import argparse
import rospy
from logger import H5Interface


if __name__ == '__main__':
    rospy.init_node('plot_trajectory')
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', dest='cmd', action='store_true')
    parser.add_argument('-i', dest='idx', type=int, default='-1')
    parser.add_argument('-f', dest='filename', type=str, default='trajectories.h5')
    args = parser.parse_args(sys.argv[1:])
    
    if args.idx < 0:
        # get latest trajectory
        path = os.path.expanduser("~/.ros/ll4ma_robot_control")
        index_file = args.filename.split('.')[0]
        with open (os.path.join(path, "." + index_file + ".index")) as f:
            idx = str(int(f.readline()) - 1)
    else:
        idx = str(args.idx)
    
    trajectory = 'trajectory_' + idx
    h = H5Interface(args.filename)
    rospy.loginfo("Plotting: " + trajectory)
    h.plot_pose_wrench(trajectory, desired=(not args.cmd))

