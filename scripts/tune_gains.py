#!/usr/bin/env python
import sys
import rospy
import numpy as np
from copy import copy
from sensor_msgs.msg import JointState


class GainTuner:

    def __init__(self, jnt_num, setpoint):
        self.jnt_num = jnt_num
        self.setpoint = setpoint
        self.robot_name = rospy.get_param("robot_name")
        self.jnt_state_topic = rospy.get_param("/%s/jnt_state_topic" % self.robot_name)
        self.jnt_des_cmd_topic = rospy.get_param("/%s/jnt_des_cmd_topic" % self.robot_name)
        self.rate_val = rospy.get_param("/%s/run_rate" % self.robot_name)
        self.jnt_names = rospy.get_param("/%s/joint_names" % self.robot_name)
        if self.jnt_num < 0 or self.jnt_num > len(self.jnt_names) - 1:
            rospy.logerr("[GainTuner] Joint number '%d' not in joint names: %s" % (self.jnt_num, self.jnt_names))
            return
        rospy.Subscriber(self.jnt_state_topic, JointState, self._joint_state_cb)
        self.jnt_des_cmd_pub = rospy.Publisher(self.jnt_des_cmd_topic, JointState, queue_size=1)
        self.rate = rospy.Rate(self.rate_val)
        self.jnt_state = None

    def run(self):
        rospy.loginfo("[GainTuner] Waiting for joint state...")
        while not rospy.is_shutdown() and self.jnt_state is None:
            self.rate.sleep()
        rospy.loginfo("[GainTuner] Joint state received.")
        
        current = list(self.jnt_state.position)
        desired = copy(current)
        desired[self.jnt_num] = self.setpoint
        trajectory = self._interpolate(current, desired)        
        cmd = JointState()
        
        rospy.loginfo("[GainTuner] Commanding to setpoint...")
        for p in trajectory:
            cmd.position = p
            self.jnt_des_cmd_pub.publish(cmd)
            self.rate.sleep()

        rospy.loginfo("[GainTuner] Complete. Holding at position for 5 seconds...")
        rospy.sleep(5.0)
        
        rospy.loginfo("[GainTuner] Complete. Commanding to original position...")
        for p in reversed(trajectory):
            cmd.position = p
            self.jnt_des_cmd_pub.publish(cmd)
            self.rate.sleep()
        rospy.loginfo("[GainTuner] Complete.")
        
    def shutdown(self):
        rospy.loginfo("[GainTuner] Exiting.")
        
    def _joint_state_cb(self, jnt_state):
        self.jnt_state = jnt_state
    
    def _interpolate(self, p1, p2, n_pts=None, multiplier=5.0):
        if n_pts is None:
            n_pts = self.rate_val * multiplier
        dims = []
        for a, b in zip(p1, p2):
            dims.append(np.linspace(a, b, n_pts))
        return [pt for pt in zip(*dims)]
    

if __name__ == '__main__':
    rospy.init_node('baxter_move_to_position')
    args = rospy.myargv(argv=sys.argv[1:])
    if len(args) != 2:
        rospy.logerr("[GainTuner] Must provide joint number (int) and setpoint (float).")
    else:
        gt = GainTuner(int(args[0]), float(args[1]))
        rospy.on_shutdown(gt.shutdown)
        gt.run()
