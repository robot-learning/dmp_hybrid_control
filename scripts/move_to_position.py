#!/usr/bin/env python
import rospy
import numpy as np
from sensor_msgs.msg import JointState
from ll4ma_robot_control.srv import SwitchControl


POSITIONS = {
    'lbr4':
    {
        'wall_start' : [0.0, 0.62, 0.0, 1.39, 0.0, -0.80, -0.33],
        'table_start' : [0.83, 0.61, 0.04, -1.33, -0.04, 1.20, -0.69],
        'table_start_center' : [-1.55, -0.49, 0.0, 1.07, 0.0, -1.56, -0.69]
    },
    'baxter':
    {
        'neutral' : [0.0, -0.55, 0.0, 0.75, 0.0, 1.26, 0.0]
    }
}


class JointPositionCommander:

    def __init__(self):
        self.current = None
        self.robot_name = rospy.get_param("robot_name")
        self.rate = rospy.Rate(rospy.get_param("/%s/run_rate" % self.robot_name))
        self.initial_position = rospy.get_param("/%s/initial_position" % self.robot_name)
        if self.initial_position not in POSITIONS[self.robot_name].keys():
            rospy.logerr("[MoveToPosition] Unknown position: %s" % self.initial_position)
        self.control_type = rospy.get_param("/%s/control_type" % self.robot_name)
        self.jnt_des_cmd_topic = rospy.get_param("/%s/jnt_des_cmd_topic" % self.robot_name)
        self.jnt_state_topic = rospy.get_param("/%s/jnt_state_topic" % self.robot_name)
        self.jnt_des_cmd_pub = rospy.Publisher(self.jnt_des_cmd_topic, JointState, queue_size=1)
        self.jnt_state_sub = rospy.Subscriber(self.jnt_state_topic, JointState, self._joint_state_cb)

    def run(self):
        rospy.loginfo("[MoveToPosition] Waiting for service SwitchControl...")
        rospy.wait_for_service("/%s/switch_controller" % self.robot_name)
        rospy.loginfo("[MoveToPosition] Service found.")
        desired = POSITIONS[self.robot_name][self.initial_position]
        rospy.loginfo("[MoveToPosition] Waiting for current joint state...")
        while self.current is None:
            self.rate.sleep()
        trajectory = self._interpolate(self.current, desired)        
        cmd = JointState()
        rospy.loginfo("[MoveToPosition] Moving robot to initial position: %s"
                      % self.initial_position)
        for p in trajectory:
            cmd.position = p
            self.jnt_des_cmd_pub.publish(cmd)
            self.rate.sleep()
        while np.linalg.norm(np.array(desired) - np.array(self.current)) > 0.02:
            self.jnt_des_cmd_pub.publish(cmd)
            self.rate.sleep()
        rospy.loginfo("[MoveToPosition] Desired position reached. Switching controller...")
        try:
            switch_controller = rospy.ServiceProxy("/%s/switch_controller" % self.robot_name,
                                                   SwitchControl)
            resp = switch_controller(self.control_type, True)
            rospy.loginfo("[MoveToPosition] Control switched. Exiting.")
        except rospy.ServiceException, e:
            rospy.logerr("[MoveToPosition] Service call to SwitchControl failed: %s" % e)

    def _joint_state_cb(self, joint_state_msg):
        self.current = joint_state_msg.position

    def _interpolate(self, p1, p2, n_pts=5000):
        dims = []
        for a, b in zip(p1, p2):
            dims.append(np.linspace(a, b, n_pts))
        return [pt for pt in zip(*dims)]
    

if __name__ == '__main__':
    rospy.init_node('move_to_position')
    commander = JointPositionCommander()
    commander.run()
