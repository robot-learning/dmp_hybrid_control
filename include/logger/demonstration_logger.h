#ifndef LL4MA_DEMONSTRATION_LOGGER
#define LL4MA_DEMONSTRATION_LOGGER

#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <std_srvs/Empty.h>
#include <geometry_msgs/WrenchStamped.h>
#include <sensor_msgs/JointState.h>
#include <boost/scoped_ptr.hpp>
#include <kdl_parser/kdl_parser.hpp>
#include <kdl/chainfksolvervel_recursive.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/chaindynparam.hpp>
#include <baxter_core_msgs/SEAJointState.h>
#include "chainjnttojacdotsolver.hpp"
#include "logger.h"

class DemonstrationLogger
{

private:
  bool do_logging_, first_log_, last_log_, jnt_state_received_, wrench_state_received_;
  bool use_ft_sensor_, transform_wrench_;
  std::string ft_topic_, filtered_ft_topic_, jnt_state_topic_, sea_jnt_state_topic_;
  std::string db_path_, db_name_, ft_link_, root_link_, tip_link_, robot_description_;
  int ft_index_, num_jnts_;
  double num_logged_, rate_val_, time_sec_;
  std::string robot_name_;
  
  // ROS 
  ros::Subscriber jnt_state_sub_, sea_jnt_state_sub_, wrench_state_sub_, filtered_wrench_sub_;
  ros::Subscriber btn_state_sub_;
  ros::ServiceServer clear_data_srv_, save_data_srv_;
  ros::Rate rate_;

  // KDL
  KDL::Tree robot_tree_;
  KDL::Chain kdl_chain_;
  KDL::FrameVel x_xdot_;
  KDL::FrameAcc x_xdot_xdotdot_;
  KDL::JntArray G_kdl_;
  KDL::JntArrayVel q_qdot_;
  KDL::Wrench w_raw_, w_raw_base_, w_filt_, w_filt_base_;
  KDL::Vector gravity_ = KDL::Vector(0.,0.,-9.81289);
  KDL::Jacobian J_kdl_;
  KDL::Twist jdot_qdot_kdl_, x_dotdot_kdl_;
  boost::scoped_ptr<KDL::ChainDynParam> dyn_model_solver_;
  boost::scoped_ptr<KDL::ChainFkSolverVel> fk_solver_;
  boost::scoped_ptr<KDL::ChainJntToJacSolver> jac_solver_;
  boost::scoped_ptr<KDL::ChainJntToJacDotSolver> jac_dot_solver_;

  // Eigen
  Eigen::MatrixXd J_;
  Eigen::VectorXd jdot_qdot_, q_dotdot_, x_dotdot_, G_, tau_g_;

  // Logger
  TrajectoryLogger logger_;

  void buttonStateCallback(std_msgs::Bool btn_msg);
  void jointStateCallback(sensor_msgs::JointState jnt_msg);
  void wrenchStateCallback(geometry_msgs::WrenchStamped wrench_stmp);
  void seaJointStateCallback(baxter_core_msgs::SEAJointState jnt_state);
  void filteredWrenchStateCallback(geometry_msgs::WrenchStamped wrench_stmp);
  
public:
  DemonstrationLogger(double rate) : rate_(rate) {}

  bool init(ros::NodeHandle &nh);
  void run();
  bool clearData(std_srvs::Empty::Request &req, std_srvs::Empty::Response &resp);
  bool saveData(std_srvs::Empty::Request &req, std_srvs::Empty::Response &resp);

  
}; // class DemonstrationLogger

#endif
