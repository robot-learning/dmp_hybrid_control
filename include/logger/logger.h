#ifndef LL4MA_LOGGER
#define LL4MA_LOGGER

#include <map>
#include <Eigen/Geometry>
#include <kdl/frames.hpp>
#include <kdl/jntarray.hpp>
#include <kdl/jntarrayvel.hpp>
#include <kdl/framevel.hpp>
#include <kdl/frameacc.hpp>
#include <ros/ros.h>
#include <geometry_msgs/Pose.h>
#include <boost/any.hpp>
#include "H5Cpp.h"


enum DataKey
{
  X, X_DES, X_ERR, X_DOT, X_DOT_DES, X_DOT_ERR, X_DOT_DOT, X_FT,
  Q, Q_DES, Q_ERR, Q_DOT, Q_DOT_DES, Q_DOT_ERR,
  W_DES, W_DES_CF, W_DES_BASE, W_DES_TOOL,
  W_FILT, W_FILT_BASE, W_FILT_TOOL, W_FILT_CF,
  W_RAW, W_RAW_BASE, W_RAW_TOOL, W_RAW_CF,
  W_ERR,
  CONSTRAINT, TAU, TIME,
  SEA_SPRING, SEA_CROSS, SEA_GRAVITY, SEA_GRAVITY_MODEL, GRAVITY
};

enum AttributeKey
{
  START, END, RATE, ROBOT_NAME,
  K_X_P, K_X_D, K_F_P, K_F_I, K_F_V, K_NULL_P, K_NULL_D,
  INTEGRAL_DECAY, INTEGRAL_SCALING
};


class TrajectoryLogger
{
  typedef std::map<int, std::vector<double>> DimensionMap;
  typedef std::map<DataKey, DimensionMap> DataMap;
  typedef std::map<DataKey, std::vector<double>> VectorMap;
  typedef std::map<AttributeKey, std::string> StringAttributeMap;
  typedef std::map<AttributeKey, double> DoubleAttributeMap;

private:
  DataMap data_map_;
  VectorMap vector_map_;
  StringAttributeMap str_attr_map_;
  DoubleAttributeMap dbl_attr_map_;
  std::string db_path_;
  std::string db_name_;
  std::string group_name_;
  std::string index_file_;
  geometry_msgs::Pose pose_msg_;
  bool data_logged_;

  ros::NodeHandle nh_;

  // TODO get this from elsewhere, hardcoding now
  static const int NUM_JOINTS = 7;

  void setupDatabase(H5::H5File&, H5::Group&);
  std::string getNextIndexValue();
  std::string enumToString(DataKey key);
  std::string enumToString(AttributeKey key);

public:
  TrajectoryLogger() {}
  
TrajectoryLogger(std::string db_path, std::string db_name, ros::NodeHandle& nh):
  db_path_(db_path), db_name_(db_name), nh_(nh)
  {
    data_logged_ = false;
    index_file_ = db_path_ + "/." + db_name + ".index";
  }

  void registerElement(DataKey key, double &d);
  void registerElement(DataKey key, KDL::Frame &f);
  void registerElement(DataKey key, KDL::Twist &t);
  void registerElement(DataKey key, KDL::Wrench &w);
  void registerElement(DataKey key, KDL::JntArray &ja);
  void registerElement(DataKey frame_key, DataKey twist_key, KDL::FrameVel &fv);
  void registerElement(DataKey frame_key, DataKey twist_key, DataKey accel_key, KDL::FrameAcc &fa);
  void registerElement(DataKey pos_key, DataKey vel_key, KDL::JntArrayVel &jav);

  void setAttribute(AttributeKey, std::string);
  void setAttribute(AttributeKey, double);
  
  template<typename Derived>
  void registerElement(DataKey key, Eigen::MatrixBase<Derived> &v)
  {
    DimensionMap dimension_map;
    for (int i = 0; i < v.size(); i++)
    {
      std::vector<double> log_vector;
      dimension_map[i] = log_vector;
    }
    data_map_[key] = dimension_map;
    ROS_INFO_STREAM("[TrajectoryLogger] Registered new element '" << this->enumToString(key)
                    << "' of size " << data_map_[key].size());
  }

  void log(DataKey key, double &d);
  void log(DataKey key, KDL::Frame &f);
  void log(DataKey key, KDL::Twist &t);
  void log(DataKey key, KDL::Wrench &w);
  void log(DataKey key, KDL::JntArray &ja);
  void log(DataKey frame_key, DataKey twist_key, KDL::FrameVel &fv);
  void log(DataKey frame_key, DataKey twist_key, DataKey accel_key, KDL::FrameAcc &fa);
  void log(DataKey pos_key, DataKey vel_key, KDL::JntArrayVel &jav);
  
  template<typename Derived>
  void log(DataKey key, Eigen::MatrixBase<Derived> &v)
  {
    data_logged_ = true;
    if (v.size() != data_map_[key].size())
      ROS_ERROR_STREAM("[TrajectoryLogger Could not log data point, vector size " << v.size()
                       << " does not match element size " << data_map_[key].size()
                       << " for key '" << this->enumToString(key) << "'.");
    else
      for (int i = 0; i < v.size(); i++)
        data_map_[key][i].push_back(v(i));
  }
  
  void writeToDisk();
  void clearData();
};

#endif
