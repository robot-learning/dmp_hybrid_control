#ifndef LL4MA_ROBOT_INTERFACE
#define LL4MA_ROBOT_INTERFACE

#include <ros/ros.h>
#include <Eigen/Geometry>
#include <sensor_msgs/JointState.h>

namespace robot_interface
{
  class RobotInterface
  {
  protected:
    ros::NodeHandle nh_;
    ros::Publisher jnt_cmd_pub_;
    std::string jnt_cmd_topic_;
    std::vector<double> torque_lims_;
    int num_jnts_;
    std::vector<std::string> jnt_names_;

    void saturateTorques(Eigen::VectorXd &torques);
    
  public:
    RobotInterface(ros::NodeHandle &nh) : nh_(nh) {}
    virtual bool init(std::vector<std::string> &jnt_names);
    virtual void publishTorqueCommand(Eigen::VectorXd &torques);
    virtual void compensateGravity(Eigen::VectorXd &tau, Eigen::VectorXd &gravity);
    virtual void getSEAJointState(Eigen::VectorXd &spring, Eigen::VectorXd &cross, 
				  Eigen::VectorXd &gravity, Eigen::VectorXd &gravity_model);

  };
}

#endif
