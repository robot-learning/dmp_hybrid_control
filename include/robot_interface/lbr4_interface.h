#ifndef LL4MA_LBR4_INTERFACE
#define LL4MA_LBR4_INTERFACE

#include <ros/ros.h>
#include <Eigen/Geometry>
#include <sensor_msgs/JointState.h>
#include "robot_interface.h"

namespace robot_interface
{
  class LBR4Interface : public RobotInterface
  {
  protected:
    sensor_msgs::JointState jnt_cmd_;
    void initJointState(sensor_msgs::JointState &jnt_state);
    
  public:
    LBR4Interface(ros::NodeHandle &nh) : RobotInterface(nh) {}
    bool init(std::vector<std::string> &jnt_names);
    void publishTorqueCommand(Eigen::VectorXd &torques);
    void compensateGravity(Eigen::VectorXd &tau, Eigen::VectorXd &gravity);
  };
}

#endif
