#ifndef LL4MA_JS_POSITION_CONTROL
#define LL4MA_JS_POSITION_CONTROL

#include "js_control.h"


namespace controller_interface
{
  class JointSpacePositionController: public JointSpaceController
  {
  public:
  JointSpacePositionController(double rate, ros::NodeHandle &nh) : JointSpaceController(rate, nh) {}
    bool configureHook();
    bool startHook();
    void updateHook();
    void stopHook();
    void cleanupHook();
  };
}
  
#endif
