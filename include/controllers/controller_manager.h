#ifndef LL4MA_CONTROLLER_MANAGER
#define LL4MA_CONTROLLER_MANAGER

#include <ros/ros.h>
#include <boost/assign.hpp>
#include "js_position_control.h"
#include "os_hybrid_force_position_control.h"
#include "os_force_null_inv_dyn_control.h"
#include "os_inv_dyn_control.h"
#include <ll4ma_robot_control/SwitchControl.h>

enum ControlType
{
  OS_INV_DYN, OS_FORCE_NULL_INV_DYN, PARALLEL_FORCE_POSITION,
  HYBRID_FORCE_POSITION, JS_POSITION
};

namespace controller_interface
{
  class ControllerManager
  {
  private:
    ros::NodeHandle nh_;
    ros::ServiceServer srv_;
    ros::Rate rate_;
    std::map<std::string, ControlType> type_map_ = boost::assign::map_list_of
      ("os_inv_dyn", OS_INV_DYN)
      ("os_force_null_inv_dyn", OS_FORCE_NULL_INV_DYN)
      ("parallel_force_position", PARALLEL_FORCE_POSITION)
      ("hybrid_force_position", HYBRID_FORCE_POSITION)
      ("js_position", JS_POSITION);
    std::string control_type_str_;
    ControlType control_type_;
    bool switch_control_;

    JointSpacePositionController js_position_controller_;
    HybridForcePositionController hybrid_controller_;
    OpSpaceForceNullInvDynController force_null_controller_;
    OpSpaceInvDynController inv_dyn_controller_;

  public:
  ControllerManager(ros::NodeHandle &nh, double rate, bool switch_control) : nh_(nh), rate_(rate),
      js_position_controller_(rate, nh),
      hybrid_controller_(rate, nh),
      force_null_controller_(rate, nh),
      inv_dyn_controller_(rate, nh)
      {
	switch_control_ = switch_control;
      }
    bool init();
    void run();
    bool switchController(ll4ma_robot_control::SwitchControl::Request &req,
                          ll4ma_robot_control::SwitchControl::Response &res);
  };

} // namespace controller_interface

#endif
