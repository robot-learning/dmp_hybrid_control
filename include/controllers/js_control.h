#ifndef LL4MA_JS_CONTROL
#define LL4MA_JS_CONTROL

#include "base_control.h"


namespace controller_interface
{
  class JointSpaceController: public BaseController
  {
  protected:
    bool stop_control_;
    std::string jnt_des_cmd_topic_;

    // Eigen
    Eigen::VectorXd q_, q_des_, q_err_, q_dot_, q_dot_des_, q_dot_err_, u_;
    Eigen::MatrixXd Kp_, Kd_;

    // ROS
    ros::Subscriber jnt_cmd_sub_;
    void jointCmdCallback(sensor_msgs::JointState cmd_msg);
    
    void setCurrentConfigAsDesired();
    
  public:
  JointSpaceController(double rate, ros::NodeHandle &nh) : BaseController(rate, nh) {}
    bool configureHook();
    bool getStopControl();
    void setStopControl(bool status);
    void jointStateCallback(sensor_msgs::JointState state_msg);      
  };
}

#endif
