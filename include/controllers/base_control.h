#ifndef LL4MA_BASE_CONTROLLER
#define LL4MA_BASE_CONTROLLER

#include <ros/ros.h>
#include <ros/node_handle.h>
#include <sensor_msgs/JointState.h>
#include <kdl_parser/kdl_parser.hpp>
#include <kdl/jntarrayvel.hpp>
#include <kdl/chaindynparam.hpp>
#include <boost/scoped_ptr.hpp>
#include "robot_interface.h"
#include "logger.h"


namespace controller_interface
{

  class BaseController
  {
  protected:
    bool log_, is_sim_, compensate_gravity_, jnt_state_received_, system_ok_;
    std::string robot_name_, root_link_, tip_link_, robot_description_, jnt_state_topic_, db_path_, db_name_;
    double rate_val_;
    int num_jnts_;
    std::vector<std::string> jnt_names_;

    // Robot interface
    robot_interface::RobotInterface* robot_interface_;

    // Logger
    TrajectoryLogger logger_;
    
    // KDL
    KDL::Tree robot_tree_;
    KDL::Chain kdl_chain_;
    KDL::JntArrayVel q_qdot_kdl_;
    KDL::JntArray G_kdl_, C_kdl_;
    KDL::Vector gravity_ = KDL::Vector(0.,0.,-9.81289);
    boost::scoped_ptr<KDL::ChainDynParam> dyn_model_solver_;

    // Eigen
    Eigen::VectorXd G_, C_;

    // ROS
    ros::Subscriber jnt_state_sub_;
    ros::Rate rate_;
    ros::Time time_, prev_time_;
    ros::Duration dt_;
    ros::NodeHandle nh_;

    template<typename Derived>
    bool getGainsFromParamServer(std::string param, ros::NodeHandle &nh, Eigen::MatrixBase<Derived> &v)
    {
      std::vector<double> temp;
      bool success = nh.getParam(param, temp);
      for (int i = 0; i < temp.size(); i++)
        v(i, i) = temp[i];
      return success;
    }

    void initJointState(sensor_msgs::JointState&);

  public:
  BaseController(double rate, ros::NodeHandle &nh) : rate_(rate), nh_(nh)
  {
    rate_val_ = rate;
  }
    
    bool configureHook();
    virtual bool startHook() = 0;
    virtual void updateHook() = 0;
    virtual void stopHook() = 0;
    virtual void cleanupHook() = 0;
  };    
  
} // namespace controller_interface

#endif
