#ifndef LL4MA_OS_INV_DYN_CONTROL
#define LL4MA_OS_INV_DYN_CONTROL

#include "os_control.h"
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>


class OpSpaceInvDynController: public controller_interface::OpSpaceController
{
public:
OpSpaceInvDynController(double rate, ros::NodeHandle &nh) : OpSpaceController(rate, nh) {}

  bool configureHook();
  bool startHook();
  void updateHook();
  void stopHook();
  void cleanupHook();
};

#endif
