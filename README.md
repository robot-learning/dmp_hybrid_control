# Learning Task Constraints from Demonstration for Hybrid Force/Position Control

This package contains code related to our paper ["Learning Task Constraints from Demonstration for Hybrid Force/Position Control"](https://arxiv.org/abs/1811.03026) by Adam Conkey and Tucker Hermans, accepted to [Humanoids 2019](http://humanoids2019.loria.fr/). This readme file includes short descriptions of the most relevant files and instructions for compiling on your system.

## Relevant Source Overview

| File                                                                           | Description                                                                                                               |
|--------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------|
| `policy_learning/src/policy_learning/dmps/dmp.py`                              | Main DMP class unifying the transformation system, basis functions, and canonical system.                                 |
| `policy_learning/src/policy_learning/dmps/transformation_system.py`            | Our proposed ontact-aware transformation system with force feedback.                                                      |
| `policy_learning/src/policy_learning/dmps/quaternion_transformation_system.py` | Transformation system for Cartesian DMPs used for our learned dynamic constraint frame for hybrid force/position control. |
| `src/controllers/os_force_null_inv_dyn_control.cpp`                            | The operational space controller we use for hybrid force/position control.                                                |
| `src/action_server/trajectory_action_server.py`                                | The action server used to manage online DMP execution and handling phase transitions.                                     |
| `src/force_sensor/contact_classifier.py`                                       | Our sliding window contact classifier.                                                                                    |
| `src/logger/h5_interface.py`                                                   | Utility class for managing data stored in HDF5 databases.                                                                 |


## Installation

Clone this repository into the `src` folder of your catkin workspace and use `catkin_make`  or `catkin build` as usual.

There is a dependency on [HDF5](https://support.hdfgroup.org/HDF5/) for the logging framework, and pyqtgraph for interactive trajectory editing. You can get these dependencies with

```
sudo apt install libhdf5-serial-dev hdfview python-pyqtgraph
```

The external HDF5 viewer can be used with the following exemplary command:

```
hdfview trajectories.h5
```