import os
import h5py
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as colors
from copy import copy
from mpl_toolkits.mplot3d import Axes3D


class Plotter:

    def __init__(self):
        pass

    def plot_element(self, trajectory, data, element_name, des_data=None, dmp_data=None,
                     bfs_data=None, start=0, end=None):

        # trim data to specified range and include desired plot if provided
        data = data[:, start:] if end is None else data[:, start:end]
        if des_data is not None:
            des_data = des_data[:, start:] if end is None else des_data[:, start:end]
        # if dmp_data is not None:
        #     dmp_data = dmp_data[:, start:] if end is None else dmp_data[:, start:end]

        # switch on element name
        if element_name in ['w', 'w_des', 'w_raw', 'w_filt', 'w_base', 'w_des_base', 'w_raw_base', 'w_filt_base']:
            self._plot_wrench(trajectory, data, des_data, dmp_data[element_name], bfs_data)
        elif element_name == 'w_err':
            self._plot_wrench_err(trajectory, data)
        elif element_name in ['x', 'x_des', 'x_smooth']:
            self._plot_pose(trajectory, data, des_data, dmp_data, bfs_data)
        elif element_name == 'x_err':
            self._plot_pose_err(trajectory, data)
        elif element_name in ['x_dot', 'x_dot_dot', 'x_dot_smooth', 'x_dot_dot_smooth']:
            self._plot_twist(trajectory, data, des_data, dmp_data, bfs_data)
        elif element_name in ['q', 'tau']:
            self._plot_joint_position(trajectory, data, des_data, dmp_data, bfs_data)
        elif element_name == 'q_dot':
            self._plot_joint_velocity(trajectory, data, des_data, dmp_data, bfs_data)
        else:
            print 'Unknown element name:', element_name

    def plot_versus(self, trajectory, data1, data2, plot_type):
        # TODO this is hacked right now
        if plot_type == 'w':
            self._plot_wrench(trajectory, data1, data2)
        elif plot_type =='x':
            self._plot_pose(trajectory, data1, data2)
            
    def plot_quaternion(self, actual, desired):
        fig = plt.figure(figsize=(23,11))
        fig.subplots_adjust(wspace=.25)
        actual_num_dims, actual_num_pts = actual.shape
        des_num_dims, des_num_pts = desired.shape
        actual_t = np.arange(actual_num_pts)
        des_t = np.arange(des_num_pts)
        for i in range(4):
            plt.subplot(141 + i)
            plt.plot(actual_t, actual[i,:], label='actual', lw=3.0)
            plt.plot(des_t, desired[i,:], label='desired', lw=3.0)
            plt.ylim(-1.0, 1.0)
        plt.tight_layout()
        plt.show()

    def plot_pose_wrench(self, trajectory, data, des_data=None):
        pose_ylabels = ['Px', 'Py', 'Pz', 'Qx', 'Qy', 'Qz', 'Qw']
        wrench_ylabels = ['Fx', 'Fy', 'Fz', 'Tx', 'Ty', 'Tz']
        fig = plt.figure(figsize=(32,11))
        name, idx = trajectory.split('_')
        fig.canvas.set_window_title("%s %s" % (name.title(), idx))
        fig.subplots_adjust(wspace=.25)
        # plt.suptitle('Pose and Wrench', fontweight='bold', fontsize=18)
        x_data = data['x']
        w_data = data['w']
        if des_data is not None:
            x_des_data = des_data['x']
            w_des_data = des_data['w']
        num_dims, num_pts = x_data.shape
        t = np.arange(num_pts)
        for i in range(3):
            fig.add_subplot(2, 7, i+1)
            plt.plot(t, x_data[i], label='actual', lw=3.0)
            if des_data is not None:
                plt.plot(t, x_des_data[i], label='desired', lw=3.0)
            #plt.xticks(plt.xticks()[0], [int(n/1000) for n in plt.xticks()[0]])
        for i in range(3, 7):
            fig.add_subplot(2, 7, i+1)
            plt.plot(t, x_data[i], label='actual', lw=3.0)
            if des_data is not None:
                plt.plot(t, x_des_data[i], label='desired', lw=3.0)
            #plt.xticks(plt.xticks()[0], [int(n/1000) for n in plt.xticks()[0]])
            plt.ylim(-1.0, 1.0)
        if des_data is not None:
            ymin = min(min(w_data[0,:]), min(w_data[1,:]), min(w_data[2,:]),
                       min(w_des_data[0,:]), min(w_des_data[1,:]), min(w_des_data[2,:])) - 1.0
            ymax = max(max(w_data[0,:]), max(w_data[1,:]), max(w_data[2,:]),
                       max(w_des_data[0,:]), max(w_des_data[1,:]), max(w_des_data[2,:])) + 1.0
        else:
            ymin = min(min(w_data[0,:]), min(w_data[1,:]), min(w_data[2,:])) - 1.0
            ymax = max(max(w_data[0,:]), max(w_data[1,:]), max(w_data[2,:])) + 1.0
        for i in range(3):
            fig.add_subplot(2, 7, i+8)
            plt.plot(t, w_data[i], label='actual', lw=3.0)
            if des_data is not None:
                plt.plot(t, w_des_data[i], label='desired', lw=3.0)
                plt.ylim(ymin, ymax)
            #plt.xticks(plt.xticks()[0], [int(n/1000) for n in plt.xticks()[0]])
        if des_data is not None:
            ymin = min(min(w_data[3,:]), min(w_data[4,:]), min(w_data[5,:]),
                       min(w_des_data[3,:]), min(w_des_data[4,:]), min(w_des_data[5,:])) - 1.0
            ymax = max(max(w_data[3,:]), max(w_data[4,:]), max(w_data[5,:]),
                       max(w_des_data[3,:]), max(w_des_data[4,:]), max(w_des_data[5,:])) + 1.0
        else:
            ymin = min(min(w_data[3,:]), min(w_data[4,:]), min(w_data[5,:])) - 1.0
            ymax = max(max(w_data[3,:]), max(w_data[4,:]), max(w_data[5,:])) + 1.0
        for i in range(3, 6):
            fig.add_subplot(2, 7, i+8)
            plt.plot(t, w_data[i], label='actual', lw=3.0)
            if des_data is not None:
                plt.plot(t, w_des_data[i], label='desired', lw=3.0)
                plt.ylim(ymin, ymax)
            #plt.xticks(plt.xticks()[0], [int(n/1000) for n in plt.xticks()[0]])
        plt.tight_layout()
        plt.show()

    def plot_dmp_log(self, trajectory, cs, goal_pos, fb):
        fig = plt.figure(figsize=(24, 12))
        t = np.arange(goal_pos.shape[1])
        for i in range(3):
            fig.add_subplot(3, 3, i+1)
            plt.plot(t, goal_pos[i], lw=3.0)
        t = np.arange(fb.shape[1])
        for i in range(3):
            fig.add_subplot(3, 3, i+1+3)
            plt.plot(t, fb[i], lw=3.0)
        fig.add_subplot(3, 3, 7)
        t = np.arange(len(cs))
        plt.plot(t, cs, lw=3.0)
        plt.show()

    def plot_cartesian_state(self, actual, desired):
        actual_num_dims, actual_num_pts = actual.shape
        des_num_dims, des_num_pts = desired['x'].shape
        actual_t = np.arange(actual_num_pts)
        des_t = np.arange(des_num_pts)

        # TODO this is kind of a hack function just to view full cartesian state of DMP rollouts
        fig = plt.figure(figsize=(32,11))
        # plot actual versus desired pose
        for i in range(3):
            fig.add_subplot(3, 7, i+1)            
            plt.plot(actual_t, actual[i], lw=3.0)
            plt.plot(des_t, desired['x'][i], lw=3.0)
            plt.xticks(plt.xticks()[0], [int(n/500) for n in plt.xticks()[0]])
        for i in range(3, 7):
            fig.add_subplot(3, 7, i+1)            
            plt.plot(actual_t, actual[i], lw=3.0)
            plt.plot(des_t, desired['x'][i], lw=3.0)
            plt.xticks(plt.xticks()[0], [int(n/500) for n in plt.xticks()[0]])
            plt.ylim(-1, 1)

        # plot desired velocities
        for i in range(6):
            fig.add_subplot(3, 7, i+8)
            plt.plot(des_t, desired['xdot'][i], lw=3.0)
            plt.xticks(plt.xticks()[0], [int(n/500) for n in plt.xticks()[0]])
        # plot desired accelerations
        for i in range(6):
            fig.add_subplot(3, 7, i+15)
            plt.plot(des_t, desired['xdotdot'][i], lw=3.0)
            plt.xticks(plt.xticks()[0], [int(n/500) for n in plt.xticks()[0]])
        plt.tight_layout()
        plt.show()
            

    def plot_magnitude(self, actual, desired):
        fig = plt.figure(figsize=(12,10))
        actual_num_pts = len(actual)
        des_num_pts = len(desired)
        actual_t = np.arange(actual_num_pts)
        des_t = np.arange(des_num_pts)
        plt.plot(actual_t, actual, label='actual', lw=3.0)
        plt.plot(des_t, desired, label='desired', lw=3.0)
        plt.tight_layout()
        plt.show()        

    def visualize_force_normal(self, trajectory_name, phase_name='raw_data', start=0, end=None,
                               skip=1, spherical=True, axis='x'):
        data = self._get_data(trajectory_name, phase_name, 'w')
        if end is None:
            end = data.shape[1]
        data = data[:3,start:end:skip] # only use forces
        origins = np.zeros((3, data.shape[1]))
                
        fig = plt.figure()
        ax = Axes3D(fig)
        xs = []; ys = []; zs = []
        for i in range(data.shape[1]):
            x = -np.linspace(origins[0, i], data[0, i], 2)
            y = -np.linspace(origins[1, i], data[1, i], 2)
            z = -np.linspace(origins[2, i], data[2, i], 2)
            mag = np.linalg.norm(np.array([x[1], y[1], z[1]]) - np.array([x[0], y[0], z[0]]))
            xs.append(x/mag)
            ys.append(y/mag)
            zs.append(z/mag)

        if spherical:
            ax.set_xlim3d([-1.0, 1.0])
            ax.set_ylim3d([-1.0, 1.0])
            ax.set_zlim3d([-1.0, 1.0])    
        else:
            # shift the origin of each along axis to visualize evolution over time
            inc = 0.1 * skip
            for i in range(1, data.shape[1]):
                if axis == 'x':
                    xs[i] += inc
                elif axis == 'y':
                    ys[i] += inc
                else:
                    zs[i] +=inc
                inc += (0.1 * skip)
            if axis == 'x':
                ax.set_xlim3d([0.0, (end - start) * 0.1])
                ax.set_ylim3d([-1.0, 1.0])
                ax.set_zlim3d([0.0, 1.0])
            elif axis == 'y':
                ax.set_xlim3d([-1.0, 1.0])
                ax.set_ylim3d([0.0, (end - start) * 0.1])
                ax.set_zlim3d([0.0, 1.0])
            else:
                ax.set_xlim3d([-1.0, 1.0])
                ax.set_ylim3d([0.0, 1.0])
                ax.set_zlim3d([0.0, (end - start) * 0.1])

        cnorm = colors.Normalize(vmin=0, vmax=data.shape[1])
        cmap = plt.get_cmap('gnuplot')
        smap = cm.ScalarMappable(norm=cnorm, cmap=cmap)
        for i in range(data.shape[1]):
            color = smap.to_rgba(i)
            ax.plot(xs[i], ys[i], zs[i], color=color)
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')
        plt.show()

    # TODO this is TEMPORARY for debugging gravity comp
    def plot_baxter_gravity(self, my_gravity, sea_spring, sea_cross, sea_gravity, sea_gravity_model):
        ylabels=['q0', 'q1', 'q2', 'q3', 'q4', 'q5', 'q6']
        fig = plt.figure(figsize=(23,11))
        fig.subplots_adjust(wspace=.25)
        plt.suptitle('Gravity Torques', fontweight='bold', fontsize=18)
        num_dims, num_pts = my_gravity.shape
        t = np.arange(num_pts)
        for i in range(7):
            ax = plt.subplot(241 + i)
            plt.plot(t, my_gravity[i], label='my_gravity', lw=3.0)
            plt.plot(t, sea_spring[i], label='sea_spring', lw=3.0)
            # plt.plot(t, sea_cross[i], label='sea_cross', lw=3.0)
            plt.plot(t, sea_gravity[i], label='sea_gravity', lw=3.0)
            plt.plot(t, sea_gravity_model[i], label='sea_gravity_model', lw=3.0)
            plt.plot(t, sea_gravity[i] - sea_spring[i], label='sea_grav - spring', lw=3.0)
            plt.plot(t, my_gravity[i] - sea_spring[i], label='my_grav - spring', lw=3.0)
            plt.ylabel(ylabels[i], fontsize=16)
        # box = ax.get_position()
        # ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        leg = ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        for line in leg.get_lines():
            line.set_linewidth(6)
        plt.show()


        
    def _plot_wrench(self, trajectory, data, des_data=None, dmp_data=None, bfs_data=None):
        ylabels=['Fx', 'Fy', 'Fz', 'Tx', 'Ty', 'Tz']
        fig = plt.figure(figsize=(22,12))
        name, idx = trajectory.split('_')
        fig.canvas.set_window_title("%s %s" % (name.title(), idx))
        fig.subplots_adjust(wspace=.25)
        plt.suptitle('Wrench', fontweight='bold', fontsize=18)
        num_dims, num_pts = data.shape
        t = np.arange(num_pts)
        ymin = np.min(data[:3]) - 10
        ymax = np.max(data[:3]) + 10
        for i in range(3):
            plt.subplot(231 + i)
            # plt.ylim(ymin, ymax)
            plt.plot(t, data[i], label='actual')
            if des_data is not None:
                plt.plot(t, des_data[i], label='desired')
            if dmp_data is not None:
                plt.plot(t, dmp_data[i], label='dmp', lw=3.0, c='r')
            plt.ylabel(ylabels[i], fontsize=16)
            plt.legend()
        ymin = np.min(data[3:]) - 0.2
        ymax = np.max(data[3:]) + 0.2
        for i in range(3, 6):
            plt.subplot(231 + i)
            plt.ylim(ymin, ymax)
            plt.plot(t, data[i], label='actual')
            if des_data is not None:
                plt.plot(t, des_data[i], label='desired')
            plt.ylabel(ylabels[i], fontsize=16)
        plt.show()

    def _plot_wrench_err(self, trajectory, data):
        ylabels=['Fx', 'Fy', 'Fz', 'Tx', 'Ty', 'Tz']
        fig = plt.figure(figsize=(22,12))
        fig.subplots_adjust(wspace=.25)
        plt.suptitle('Wrench', fontweight='bold', fontsize=18)
        num_dims, num_pts = data.shape
        t = np.arange(num_pts)
        ymin = np.min(data[:3]) - 10
        ymax = np.max(data[:3]) + 10
        for i in range(6):
            plt.subplot(231 + i)
            plt.plot(t, data[i])
            plt.ylabel(ylabels[i], fontsize=16)
        plt.show()
        
    def _plot_pose(self, trajectory, data, des_data=None, dmp_data=None, bfs_data=None, duration=None):
        ylabels=['Px', 'Py', 'Pz', 'Qx', 'Qy', 'Qz', 'Qw']
        fig = plt.figure(figsize=(23,11))
        name, idx = trajectory.split('_')
        fig.canvas.set_window_title("%s %s" % (name.title(), idx))
        fig.subplots_adjust(wspace=.25)
        plt.suptitle('Pose', fontweight='bold', fontsize=18)
        num_dims, num_pts = data.shape
        t = np.arange(num_pts) if duration is None else np.linspace(0.0, duration, int(num_pts))
        ymin = np.min(data[:3]) - 0.2
        ymax = np.max(data[:3]) + 0.2
        for i in range(3):
            plt.subplot(241 + i)
            plt.plot(t, data[i], label='actual', lw=3.0)
            if des_data is not None:
                plt.plot(t, des_data[i], label='desired', lw=3.0)
            if dmp_data is not None:
                plt.plot(t, dmp_data[i], label='dmp', lw=3.0, c='r')
            plt.ylabel(ylabels[i], fontsize=16)
        for i in range(3, 7):
            plt.subplot(241 + i + 1)
            plt.plot(t, data[i], label='actual')
            plt.ylim(-1.0, 1.0)
            if des_data is not None:
                plt.plot(t, des_data[i], label='desired')
            if dmp_data is not None:
                plt.plot(t, dmp_data[i], label='dmp', lw=3.0, c='r')
            plt.ylabel(ylabels[i], fontsize=16)
        if bfs_data:
            fig = plt.figure(figsize=(24,8))
            for i in range(3):
                plt.subplot(131 + i)
                for j in range(len(bfs_data[i][0])): # (bfs, weights)
                    # plt.plot(t, bfs_data[i][1][j] * bfs_data[i][0][j,:], lw=2.0)
                    plt.plot(t, bfs_data[i][0][j,:], lw=2.0)
        plt.tight_layout()
        plt.show()

    def _plot_pose_err(self, trajectory, data):
        ylabels=['Tx', 'Ty', 'Tz', 'Rx', 'Ry', 'Rz']
        fig = plt.figure(figsize=(22,12))
        fig.subplots_adjust(wspace=.25)
        plt.suptitle('Pose Error', fontweight='bold', fontsize=18)
        num_dims, num_pts = data.shape
        t = np.arange(num_pts)
        for i in range(6):
            plt.subplot(231 + i)
            plt.plot(t, data[i])
            plt.ylabel(ylabels[i], fontsize=16)
        plt.show()
            
    def _plot_twist(self, trajectory, data, des_data=None, dmp_data=None, bfs_data=None):
        ylabels=['Vx', 'Vy', 'Vz', 'Wx', 'Wy', 'Wz']
        fig = plt.figure(figsize=(22,12))
        fig.subplots_adjust(wspace=.25)
        plt.suptitle('Twist', fontweight='bold', fontsize=18)
        num_dims, num_pts = data.shape
        t = np.arange(num_pts)
        for i in range(6):
            plt.subplot(231 + i)
            plt.plot(t, data[i], label='actual')
            if des_data is not None:
                plt.plot(t, des_data[i], label='desired')
            if dmp_data is not None:
                plt.plot(t, dmp_data[i], label='dmp')
            plt.ylabel(ylabels[i], fontsize=16)
        plt.show()
    
    def _plot_joint_position(self, trajectory, data, des_data=None, dmp_data=None, bfs_data=None):
        ylabels=['q0', 'q1', 'q2', 'q3', 'q4', 'q5', 'q6']
        fig = plt.figure(figsize=(30,14))
        fig.subplots_adjust(wspace=.25)
        plt.suptitle('Joint Position', fontweight='bold', fontsize=18)
        num_dims, num_pts = data.shape
        t = np.arange(num_pts)
        for i in range(7):
            plt.subplot(241 + i)
            plt.plot(t, data[i], label='actual')
            if des_data is not None:
                plt.plot(t, des_data[i], label='desired', lw=3.0)
            if dmp_data is not None:
                plt.plot(t, dmp_data[i], label='dmp')
            plt.ylabel(ylabels[i], fontsize=16)
        plt.show()

    def _plot_joint_velocity(self, trajectory, data, des_data=None, dmp_data=None, bfs_data=None):
        ylabels=['q0', 'q1', 'q2', 'q3', 'q4', 'q5', 'q6']
        fig = plt.figure(figsize=(23,11))
        fig.subplots_adjust(wspace=.25)
        plt.suptitle('Joint Velocity', fontweight='bold', fontsize=18)
        num_dims, num_pts = data.shape
        t = np.arange(num_pts)
        for i in range(7):
            plt.subplot(241 + i)
            plt.plot(t, data[i], label='actual')
            if des_data is not None:
                plt.plot(t, des_data[i], label='desired')
            if dmp_data is not None:
                plt.plot(t, dmp_data[i], label='dmp')
            plt.ylabel(ylabels[i], fontsize=16)
        plt.show()
