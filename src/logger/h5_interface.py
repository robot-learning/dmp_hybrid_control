import os
import sys
import h5py
import rospy
import numpy as np
from shutil import copyfile
from logger import Plotter, TrajectoryWidget
from force_sensor import ContactClassifier
from policy_learning import TrajectoryLearner
from policy_learning.dmps import DynamicMovementPrimitive
from action_server import TrajectoryActionClient
from geometry_msgs.msg import Pose, PoseStamped, Wrench


class H5Interface:

    def __init__(self, h5_filename='trajectories.h5', path='~/.ros/ll4ma_robot_control',
                 trajectory_client=False, rospy_init=False, rospy_init_tc=True):
        self.h5_filename = h5_filename
        self._set_h5_path(path)
        self._set_h5_abspath(self.h5_filename)
        self.h5_file = h5py.File(self.h5_abspath, 'r+')
        self._plotter = Plotter()
        self._learner = TrajectoryLearner()
        self._widget = TrajectoryWidget()
        # TODO pass these params in with H5Interface constructor? Better defaults?
        self._classifier = ContactClassifier(robot_name="", window_size=50, mu_noise=1.0)
        if trajectory_client:
            self._trajectory_client = TrajectoryActionClient(rospy_init=rospy_init_tc)
        if rospy_init:
            rospy.init_node('H5Interface')
            
    def learn_dmps(self, trajectory, phase='full', elements=['x'], **kwargs):
        self._backup_database()
        data = self.get_data(trajectory)
        if phase == 'full' and phase not in data.keys():
            # learning DMPs for full trajectory without phases
            data.create_group(phase)
            self.h5_file.flush()
            data[phase].attrs.create('start', data['raw_data'].attrs['start'], dtype=np.float64)
            data[phase].attrs.create('end', data['raw_data'].attrs['end'], dtype=np.float64)
            data[phase].attrs.create('type', 'full_motion')
            self.h5_file.flush()
        start = int(data[phase].attrs['start'])
        end = int(data[phase].attrs['end'])
        params, rollout_data = self._learner.learn_dmps(data['raw_data'], start, end,
                                                              elements, **kwargs)
        self._view_dmp_rollout(trajectory, rollout_data, start=start, end=end)
        msg = "\nWould you like to save these parameters to the database?"
        save_params = self._query_yes_no(msg, default="n")
        if save_params:
            self._save_dmp_params(params, trajectory, phase)
        else:
            self._log("\nNOT saving DMP parameters.")
            
    def learn_constraint_frame(self, trajectory, phase, rviz=False, w_type='w_filt',
                               x_type='x_ft',**kwargs):
        data = self.get_data(trajectory)
        start = int(data[phase].attrs['start'])
        end = int(data[phase].attrs['end'])
        params, attrs, rollout_data, qs, mags = self._learner.learn_constraint_frame(
            data['raw_data'][w_type][:3,start:end],
            data['raw_data']['w_filt_base'][:3,start:end], **kwargs)
        self._plotter.plot_quaternion(rollout_data['cf']['rot'], qs)
        self._plotter.plot_magnitude(rollout_data['cf']['mag'], mags)
        if rviz:
            # read in positions from EE pose
            xs = data['raw_data']['x'][:,start:end]
            self.visualize_constraint_frame(rollout_data, qs)
        msg = "\nWould you like to save these parameters to the database?"
        save_params = self._query_yes_no(msg, default="n")
        msg = "\nWould you like to save the rollout data to the database?"
        save_rollouts = self._query_yes_no(msg, default="n")
        if save_params:
            self._save_dmp_params(params, trajectory, phase, attrs)
        if save_rollouts:
            self.save_data(trajectory, phase, 'rollouts/qs', qs)
            self.save_data(trajectory, phase, 'rollouts/mag', mags)
            self.save_data(trajectory, phase, 'rollouts/cf_qs', rollout_data['cf']['rot'])
            self.save_data(trajectory, phase, 'rollouts/cf_mags', rollout_data['cf']['mag'])
        else:
            self._log("\nNOT saving DMP parameters.")

    def visualize_constraint_frame(self, rollout_data, qs, loops=3, frame="push_ball_center"):
        cf_viz_pub = rospy.Publisher("/visualize_constraint_frame", PoseStamped, queue_size=1)
        cf_qs = rollout_data['cf']['rot']
        cf_p = PoseStamped()
        cf_p.header.frame_id = frame

        raw_viz_pub = rospy.Publisher("/visualize_raw_constraint_frame", PoseStamped, queue_size=1)
        raw_p = PoseStamped()
        raw_p.header.frame_id = frame

        rate = rospy.Rate(500)        
        rospy.loginfo("Visualizing constraint frame...")
        for i in range(loops):
            rospy.loginfo("  Loop %d of %d" % (i+1, loops))
            for j in range(cf_qs.shape[1]):
                cf_p.header.stamp = rospy.Time.now()
                cf_p.pose.orientation.x = cf_qs[0,j]
                cf_p.pose.orientation.y = cf_qs[1,j]
                cf_p.pose.orientation.z = cf_qs[2,j]
                cf_p.pose.orientation.w = cf_qs[3,j]
                cf_viz_pub.publish(cf_p)

                raw_p.header.stamp = rospy.Time.now()
                raw_p.pose.orientation.x = qs[0,j]
                raw_p.pose.orientation.y = qs[1,j]
                raw_p.pose.orientation.z = qs[2,j]
                raw_p.pose.orientation.w = qs[3,j]
                raw_viz_pub.publish(raw_p)
                
                rate.sleep()
        rospy.loginfo("Complete.")

    def set_dmp_param(self, trajectory, phase, element=None, dim=None,
                      param=None, value=None):
        if param is None or value is None:
            self._log("\nMust define parameter and value to set.")
            return
        self._backup_database()
        if element is None and dim is None:
            self._log("\nSetting %s=%.3f for '%s' of '%s'..." % (param, value, phase, trajectory))
        elif dim is None:
            self._log("\nSetting %s=%.3f for element '%s' in '%s' of '%s'..."
                      % (param, value, element, phase, trajectory))
        else:
            self._log("\nSetting %s=%.3f for dim=%s of element '%s' in '%s' of '%s'..."
                   % (param, value, dim, element, phase, trajectory))
        phase_params = self.get_data(trajectory, phase)
        element_keys = phase_params.keys() if element is None else [element]
        for element_key in element_keys:
            dim_keys = phase_params[element_key].keys() if dim is None else [dim]
            for dim_key in dim_keys:
                phase_params[element_key][dim_key].attrs.modify(param, value)
        self._log("Complete.")

    def view_dmp_params(self, trajectory, phase, element=None, plot=False):
        phase_params = self._load_dmp_params(trajectory, phase)
        np.set_printoptions(precision=3)
        if element is not None:
            element_keys = [element]
        else:
            element_keys = phase_params.keys()
        for element_key in element_keys:
            dims = phase_params[element_key]
            for dim_key in sorted(dims.keys()):
                params = dims[dim_key]
                DynamicMovementPrimitive.print_params(params,
                                                      "%s_%s_%s" % (phase, element_key, dim_key))
                if plot:
                    data = self.get_data(trajectory, 'raw_data', element_key)
                    num_pts = data.shape[1]
                    rollout_data = self._learner.get_rollouts(phase_params, num_pts)
                    self._view_dmp_rollout(trajectory, rollout_data)

    def select_phases(self, trajectory, w_type='w_raw'):
        # TODO assuming for simplicity right now that there is only one discernible
        # contact region. This can easily be changed but will have to allow for arbitrary
        # number of linear regions in GUI, which will require more intelligent way of
        # generating update functions for the GUI. For now just taking region with
        # greatest extent.
        data = self.get_data(trajectory)
        phases = ['phase_1', 'phase_2', 'phase_3']
        has_phases = True
        for p in phases:
            if p not in data.keys():
                has_phases = False
                break
     
        # pass existing params if already set, otherwise start with suggestions
        if has_phases:
            self._log("Displaying segmentation points loaded from DB.")
            existing = {}
            existing['start'] = data['phase_1'].attrs['start']
            existing['end'] = data['phase_3'].attrs['end']
            existing['c_start'] = data['phase_2'].attrs['start']
            existing['c_end'] = data['phase_2'].attrs['end']
            params = self._widget.launch(data['raw_data'], existing, w_type)
        else:
            suggested = {}
            self._log("Determining contact regions for data...")
            contact_regions = self._classifier.get_contact_regions(data['raw_data'][w_type])
            print "Complete."
            if contact_regions:
                region = contact_regions[0]
                # find region of greatest extent
                if len(contact_regions) > 1:
                    extent = region[1] - region[0]
                    for r in contact_regions:
                        if r[1] - r[0] > extent:
                            region = r
                            extent = r[1] - r[0]
                suggested = {'start':0, 'end':data['raw_data']['x'].shape[1]}
                suggested['c_start'] = region[0]
                suggested['c_end'] = region[1]
                self._log("Displaying suggested segmentation points.")
            else:
                self._log("Displaying default segmentation points.")
            params = self._widget.launch(data['raw_data'], suggested, w_type)
        if params:
            for phase in params.keys():
                try:
                    data.create_group(phase)
                except (ValueError):
                    pass # group already exists
                for key in params[phase].keys():
                    data[phase].attrs.create(key, params[phase][key], dtype=np.float64)
                # TODO simplifying for now, have better way of labelling contact regions
                if phase == 'phase_1':
                    data[phase].attrs.create('type', 'making_contact')
                elif phase == 'phase_2':
                    data[phase].attrs.create('type', 'in_contact')
                elif phase == 'phase_3':
                    data[phase].attrs.create('type', 'breaking_contact')
                else:
                    self._log("Unknown phase name: ", phase)
        self.h5_file.flush()

    def command_trajectory(self, trajectory, phase=None):
        data = self.get_data(trajectory)
        success = self._trajectory_client.set_trajectory_goal(data, phase)
        if success:
            self._trajectory_client.send_goal()
        else:
            self._log("Goal setting failed. Goal not sent to action server.")

    def command_joint_trajectory(self, trajectory, dataset='raw_data', max_time=60.0):
        data = self.get_data(trajectory)
        success = self._trajectory_client.set_joint_trajectory_goal(data)
        if success:
            self._trajectory_client.send_joint_goal()
        else:
            self._log("Goal setting failed. Goal not sent to action server.")

    def command_dmp_trajectory(self, trajectory, phases=[]):
        data = self.get_data(trajectory)
        # command the whole trajectory if no phases were specified
        if not phases:
            phases = ['full']
        phase_params = {}
        phase_attrs = {}
        phase_types = {}
        for i, phase in enumerate(phases):
            phase_params[phase] = self._load_dmp_params(trajectory, phase)
            phase_types[phase] = data[phase].attrs['type'] if phase != 'full' else 'free_space'
            # TODO hardcoding
            if phase == 'phase_1' and 'force_init_base' in data['phase_2'].attrs.keys():
                phase_attrs[phase] = {}
                phase_attrs[phase]['force_goal'] = data['phase_2'].attrs['force_init_base']
                phase_attrs[phase]['make_contact_cf'] = data['phase_2']['cf']['rot'].attrs['init']
                phase_attrs[phase]['make_contact_mag'] = data['phase_2']['cf']['mag'].attrs['init']
        success = self._trajectory_client.set_dmp_goal(trajectory, phase_params, phase_attrs, phase_types)
        if success:
            self._trajectory_client.send_goal()
        else:
            self._log("Goal setting failed. Goal not sent to action server.")

    def plot_element(self, trajectory, element, dataset='raw_data', desired=False,
                     dmp_data=None, bfs_data=None, start=0, end=None):
        data = self.get_data(trajectory, dataset, element)
        des_data = None
        if desired:
            if element in ['w_raw', 'w_trans', 'w_filt']:
                des_data = self.get_data(trajectory, dataset, 'w_des')
            else:
                des_data = self.get_data(trajectory, dataset, element + '_des')
        self._plotter.plot_element(trajectory, data, element, des_data, dmp_data, bfs_data, start, end)

    def plot_pose_wrench(self, trajectory, dataset='raw_data', desired=True):
        data = self.get_data(trajectory, dataset)
        actual_data = {}
        actual_data['x'] = data['x']
        actual_data['w'] = data['w_filt_base']
        des_data = None
        if desired:
            des_data = {}
            des_data['x'] = data['x_des']
            des_data['w'] = data['w_des_base']
        self._plotter.plot_pose_wrench(trajectory, actual_data, des_data)

    def plot_dmp_log(self, trajectory):
        data = self.get_data(trajectory)
        cs = data['cs']
        goal_pos = data['goal']['x']
        fb = data['fb']['x']
        self._plotter.plot_dmp_log(trajectory, cs, goal_pos, fb)

    def plot_versus(self, trajectory, element_1, element_2, plot_type):
        data = self.get_data(trajectory, 'raw_data')
        e1_data = data[element_1]
        e2_data = data[element_2]
        self._plotter.plot_versus(trajectory, e1_data, e2_data, plot_type)

    # TODO this is a TEMPORARY function for debugging
    def plot_baxter_gravity(self, trajectory):
        data = self.get_data(trajectory, 'raw_data')
        my_gravity = data['gravity']
        sea_spring = data['sea_spring']
        sea_cross = data['sea_cross']
        sea_gravity = data['sea_gravity']
        sea_gravity_model = data['sea_gravity_model']
        self._plotter.plot_baxter_gravity(my_gravity, sea_spring, sea_cross, sea_gravity, sea_gravity_model)

    def add_constraints(self, trajectory, phase, constraints=[0,0,0,0,0,0]):
        self.save_data(trajectory, phase, 'constraints', np.array(constraints))
        
    def delete(self, trajectory, dataset=None, element=None, validate=True):
        self._backup_database()
        user_sure = True
        if element is None and dataset is None:
            if validate:
                user_sure = self._query_yes_no("\nAre you sure you want to delete '%s'?"
                                               % trajectory, "n")
            if user_sure:
                print "\nDeleting '%s' from database..." % trajectory
                try:
                    del self.h5_file[trajectory]
                    print "Complete."
                except (KeyError):
                    print "'%s' does not exist in database. No action taken." % trajectory
            else:
                print "\nNo action taken."
        elif element is None:
            if validate:
                user_sure = self._query_yes_no("\nAre you sure you want to delete '%s' from '%s'?"
                                               % (dataset, trajectory), "n")
            if user_sure:
                print "\nDeleting '%s' from '%s'..." % (dataset, trajectory)
                try:
                    del self.h5_file[trajectory][dataset]
                    print "Complete."
                except (KeyError):
                    print ("'%s' does not exist in '%s'. No action taken."
                           % (dataset, trajectory))
            else:
                print "\nNo action taken."
        else:
            if validate:
                user_sure = self._query_yes_no("\nAre you sure you want to delete '%s' of '%s' "
                                               "from of '%s'?" % (element, dataset, trajectory), "n")
            if user_sure:
                print ("\nDeleting element '%s' from '%s' of '%s'..."
                       % (element, dataset, trajectory))
                try:
                    del self.h5_file[trajectory][dataset][element]
                    print "Complete."
                except (KeyError):
                    print ("'%s' does not exist in '%s' of '%s'. No action taken."
                           % (element, dataset, trajectory))
            else:
                print "\nNo action taken."

    def delete_attributes(self, trajectory, attributes):
        self._backup_database()
        if not isinstance(attributes, list):
            attributes = [attributes]
        for attribute in attributes:
            self._log("Deleting attribute '%s' from '%s'..." % (attribute, trajectory))
            grp = self.get_data(trajectory, 'raw_data')
            try:
                del grp.attrs[attribute]
                self._log("Success.")
            except (KeyError):
                self._log("Attribute '%s' does not exist in '%s'. No action taken."
                          % (attribute, trajectory))

    def delete_phases(self, trajectory):
        self._backup_database()
        user_sure = self._query_yes_no("\nAre you sure you want to delete all phases from '%s'"
                                       % trajectory, "n")
        if user_sure:
            data = self.get_data(trajectory)
            for key in data.keys():
                if 'phase' in key:
                    self.delete(trajectory, key, validate=False)
        else:
            print "No action taken."

    def list(self, trajectory, dataset=None):
        data = self.get_data(trajectory)
        print
        if dataset is not None:
            data = data[dataset]
        for k in data.keys():
            print k

    def list_attributes(self, trajectory, phase='raw_data'):
        # TODO incorporate into 'list' function so that it lists both attributes and datasets
        grp = self.get_data(trajectory, phase)
        print ""
        for attr in grp.attrs.keys():
            print "%s : %s" % (attr, grp.attrs[attr])
    
    def get_data(self, trajectory, dataset=None, element=None):
        try:
            data = self.h5_file[trajectory]
        except (KeyError):
            raise KeyError("Trajectory '%s' does not exist." % trajectory)
        if dataset is not None:
            try:
                data = data[dataset]
            except (KeyError):
                raise KeyError("Dataset '%s' does not exist in trajectory '%s'"
                               % (dataset, trajectory))
        if element is not None:
            try:
                data = data[element]
            except (KeyError):
                raise KeyError("Element '%s' does not exist in dataset '%s' of trajectory '%s'"
                               % (element, dataset, trajectory))
        return data
                
    def save_data(self, trajectory, dataset='raw_data', element=None, data=None):
        if data is None:
            raise ValueError("Must provide data to be saved.")
        if element is None:
            raise ValueError("Must provide element name for data to be saved to.")
        self._backup_database()
        exists = False
        try:
            self.h5_file.create_dataset("/%s/%s/%s" % (trajectory, dataset, element),
                                        data=data)
        except (RuntimeError):
            print ("Dataset exists, NOT overwriting: /%s/%s/%s"
                   % (trajectory, dataset, element))
            exists = True
        if not exists:
            print ("\nAdded element to /%s/%s/%s" % (trajectory, dataset, element))
            
    def _save_attributes(self, trajectory, params):
        self._backup_database()
        grp = self.get_data(trajectory, 'raw_data')
        for param_key in params.keys():
            grp.attrs[param_key] = params[param_key]

    def _load_attributes(self, trajectory):
        grp = self.get_data(trajectory, 'raw_data')
        return grp.attrs

    def _save_dmp_params(self, params, trajectory, phase, attrs=None):
        self._backup_database()
        for element_key in params.keys():
            print ("\nAdding DMP parameters to /%s/%s/%s..."
                   % (trajectory, phase, element_key))
            dims = params[element_key]
            for dim_key in dims.keys():
                dim_params = dims[dim_key]
                # create a dataset for the weight vector
                dset = None
                try:
                    dset = self.h5_file.create_dataset(
                        "/%s/%s/%s/%s/%s" % (trajectory, phase, element_key, dim_key, "w"),
                        data=dim_params['w'])
                except (RuntimeError):
                    print ("        Dataset exists, NOT overwriting: /%s/%s/%s/%s/%s"
                           % (trajectory, phase, element_key, dim_key, "w"))
                if dset is not None:
                    # save any phase attributes if they exist
                    if attrs:
                        grp = self.h5_file[trajectory][phase]
                        for attr_key in attrs.keys():
                            grp.attrs[attr_key] = attrs[attr_key]
                    grp = self.h5_file[trajectory][phase][element_key][str(dim_key)]
                    # attach all associated parameters to the dataset
                    for param_key in dim_params.keys():
                        if param_key == 'w':
                            continue
                        grp.attrs[param_key] = dim_params[param_key]
                print "Complete."
        
    def _load_dmp_params(self, trajectory, phase):
        phase_data = self.get_data(trajectory, phase)
        element_params = {}
        for element_key in phase_data.keys():
            if element_key not in ['rollouts']:
                element_data = phase_data[element_key]
                dim_params = {}
                for dim_key in element_data.keys():
                    dim_data = element_data[dim_key]
                    params = {}
                    params['tau'] = dim_data.attrs['tau']
                    params['alpha'] = dim_data.attrs['alpha']
                    params['beta'] = dim_data.attrs['beta']
                    params['gamma'] = dim_data.attrs['gamma']
                    params['alpha_c'] = dim_data.attrs['alpha_c']
                    params['alpha_nc'] = dim_data.attrs['alpha_nc']
                    params['alpha_p'] = dim_data.attrs['alpha_p']
                    params['alpha_f'] = dim_data.attrs['alpha_f']
                    params['init'] = dim_data.attrs['init']
                    params['goal'] = dim_data.attrs['goal']
                    params['w'] = dim_data['w'][:]
                    params['num_bfs'] = dim_data.attrs['num_bfs']
                    params['num_pts'] = dim_data.attrs['num_pts']
                    params['dt'] = dim_data.attrs['dt']
                    dim_params[dim_key] = params
                element_params[element_key] = dim_params
        return element_params

    def _view_dmp_rollout(self, trajectory, rollout_data, start=0, end=None):
        for element_key in rollout_data.keys():
            if element_key == 'x':
                data = self.get_data(trajectory, 'raw_data', 'x')[:,start:end]
                self._plotter.plot_cartesian_state(data, rollout_data['x'])
            else:
                rollout = rollout_data[element_key]
                self.plot_element(trajectory, element_key, dmp_data=rollout, start=start, end=end)

    def _view_cartesian_dmp_rollout(self, data, rollout_data):
        self._plotter.plot_cartesian_state(data, rollout_data)
    
    def _backup_database(self):
        copyfile(self.h5_abspath, os.path.join(self.h5_path, '.' + self.h5_filename + '.BACKUP'))
    
    def _set_h5_path(self, path):
        self.h5_path = os.path.abspath(os.path.expanduser(path))
        if not os.path.exists(self.h5_path):
            print "Path does not exist: %s" % self.h5_path
        
    def _set_h5_abspath(self, h5_filename):
        self.h5_abspath = os.path.join(self.h5_path, h5_filename)
        if not os.path.exists(self.h5_abspath):
            print "File does not exist: %s" % self.h5_abspath

    def _query_yes_no(self, question, default="y"):
        valid = {"y": True, "n": False}
        if default == "y":
            prompt = " ([y]/n) "
        elif default == "n":
            prompt = " (y/[n]) "
        else:
            raise ValueError("Invalid default answer: '%s'" % default)

        while True:
            sys.stdout.write(question + prompt)
            choice = raw_input().lower()
            if default is not None and choice == '':
                return valid[default]
            elif choice in valid:
                return valid[choice]
            else:
                sys.stdout.write("Please respond with 'y' or 'n'.\n")

    def _log(self, msg):
        print "[%s] %s" % (self.__class__.__name__, msg)
