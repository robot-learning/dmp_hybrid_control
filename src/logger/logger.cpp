#include "logger.h"
#include <fstream>
#include <iostream>
#include <boost/filesystem.hpp>
#include <kdl_conversions/kdl_msg.h>


void TrajectoryLogger::registerElement(DataKey key, double &d)
{
  std::vector<double> log_vector;
  vector_map_[key] = log_vector;
  ROS_INFO_STREAM("[TrajectoryLogger] Registered new element '" << this->enumToString(key) << "'");
}


void TrajectoryLogger::registerElement(DataKey key, KDL::Frame &f)
{
  Eigen::Matrix<double, 7, 1> frame_element;
  registerElement(key, frame_element);
}


void TrajectoryLogger::registerElement(DataKey frame_key, DataKey twist_key, KDL::FrameVel &fv)
{
  Eigen::Matrix<double, 7, 1> frame_element;
  Eigen::Matrix<double, 6, 1> twist_element;
  registerElement(frame_key, frame_element);
  registerElement(twist_key, twist_element);
}


void TrajectoryLogger::registerElement(DataKey frame_key, DataKey twist_key, DataKey accel_key, KDL::FrameAcc &fa)
{
  Eigen::Matrix<double, 7, 1> frame_element;
  Eigen::Matrix<double, 6, 1> twist_element;
  Eigen::Matrix<double, 6, 1> accel_element;
  registerElement(frame_key, frame_element);
  registerElement(twist_key, twist_element);
  registerElement(accel_key, accel_element);
}


void TrajectoryLogger::registerElement(DataKey pos_key, DataKey vel_key, KDL::JntArrayVel &jav)
{
  Eigen::Matrix<double, NUM_JOINTS, 1> pos_element;
  Eigen::Matrix<double, NUM_JOINTS, 1> vel_element;
  registerElement(pos_key, pos_element);
  registerElement(vel_key, vel_element);
}


void TrajectoryLogger::registerElement(DataKey key, KDL::Twist &t)
{
  Eigen::Matrix<double, 6, 1> twist_element;
  registerElement(key, twist_element);
}


void TrajectoryLogger::registerElement(DataKey key, KDL::Wrench &w)
{
  Eigen::Matrix<double, 6, 1> wrench_element;
  registerElement(key, wrench_element);
}


void TrajectoryLogger::registerElement(DataKey key, KDL::JntArray &ja)
{
  Eigen::Matrix<double, NUM_JOINTS, 1> jnt_element;
  registerElement(key, jnt_element);
}


void TrajectoryLogger::setAttribute(AttributeKey key, std::string value)
{
  ROS_INFO_STREAM("[TrajectoryLogger] Setting attribute value for '"
		  << enumToString(key) << ": " << value);
  str_attr_map_[key] = value;
}


void TrajectoryLogger::setAttribute(AttributeKey key, double value)
{
  ROS_INFO_STREAM("[TrajectoryLogger] Setting attribute value for '"
		  << enumToString(key) << "': " << value);
  dbl_attr_map_[key] = value;
}


void TrajectoryLogger::log(DataKey key, double &d)
{
  data_logged_ = true;
  vector_map_[key].push_back(d);
}


void TrajectoryLogger::log(DataKey key, KDL::Frame &f)
{
  data_logged_ = true;
  tf::poseKDLToMsg(f, pose_msg_);
  data_map_[key][0].push_back(pose_msg_.position.x);
  data_map_[key][1].push_back(pose_msg_.position.y);
  data_map_[key][2].push_back(pose_msg_.position.z);
  data_map_[key][3].push_back(pose_msg_.orientation.x);
  data_map_[key][4].push_back(pose_msg_.orientation.y);
  data_map_[key][5].push_back(pose_msg_.orientation.z);
  data_map_[key][6].push_back(pose_msg_.orientation.w);
}


void TrajectoryLogger::log(DataKey frame_key, DataKey twist_key, KDL::FrameVel &fv)
{
  data_logged_ = true;
  tf::poseKDLToMsg(fv.GetFrame(), pose_msg_);
  data_map_[frame_key][0].push_back(pose_msg_.position.x);
  data_map_[frame_key][1].push_back(pose_msg_.position.y);
  data_map_[frame_key][2].push_back(pose_msg_.position.z);
  data_map_[frame_key][3].push_back(pose_msg_.orientation.x);
  data_map_[frame_key][4].push_back(pose_msg_.orientation.y);
  data_map_[frame_key][5].push_back(pose_msg_.orientation.z);
  data_map_[frame_key][6].push_back(pose_msg_.orientation.w);

  for (int i = 0; i < 6; i++)
    data_map_[twist_key][i].push_back(fv.GetTwist()[i]);
}

void TrajectoryLogger::log(DataKey frame_key, DataKey twist_key, DataKey accel_key, KDL::FrameAcc &fa)
{
  data_logged_ = true;
  // log pose
  tf::poseKDLToMsg(fa.GetFrame(), pose_msg_);
  data_map_[frame_key][0].push_back(pose_msg_.position.x);
  data_map_[frame_key][1].push_back(pose_msg_.position.y);
  data_map_[frame_key][2].push_back(pose_msg_.position.z);
  data_map_[frame_key][3].push_back(pose_msg_.orientation.x);
  data_map_[frame_key][4].push_back(pose_msg_.orientation.y);
  data_map_[frame_key][5].push_back(pose_msg_.orientation.z);
  data_map_[frame_key][6].push_back(pose_msg_.orientation.w);

  for (int i = 0; i < 6; i++)
  {
    // log twist
    data_map_[twist_key][i].push_back(fa.GetTwist()[i]);
    // log acceleration
    data_map_[accel_key][i].push_back(fa.GetAccTwist()[i]);
  }
}

void TrajectoryLogger::log(DataKey key, KDL::Twist &t)
{
  data_logged_ = true;
  for (int i = 0; i < 6; i++)
    data_map_[key][i].push_back(t(i));
}


void TrajectoryLogger::log(DataKey key, KDL::Wrench &w)
{
  data_logged_ = true;
  for (int i = 0; i < 6; i++)
    data_map_[key][i].push_back(w(i));
}


void TrajectoryLogger::log(DataKey key, KDL::JntArray &ja)
{
  data_logged_ = true;
  for (int i = 0; i < NUM_JOINTS; i++)
    data_map_[key][i].push_back(ja(i));
}


void TrajectoryLogger::log(DataKey pos_key, DataKey vel_key, KDL::JntArrayVel &jav)
{
  data_logged_ = true;
  for (int i = 0; i < NUM_JOINTS; i++)
  {
    data_map_[pos_key][i].push_back(jav.q(i));
    data_map_[vel_key][i].push_back(jav.qdot(i));
  }
}


void TrajectoryLogger::writeToDisk()
{
  if (data_logged_) {
    H5std_string data_name;
    H5std_string dataset_name;
    H5::H5File file;
    H5::Group group;
    H5::DataSet dataset;
    H5::DataSpace dataspace;
    std::vector<double>::const_iterator it;
    int num_rows;
    int num_cols;
    hsize_t dims[2];
    int counter;
    
    setupDatabase(file, group);
    
    // log data from DataMap
    for (auto const &m1 : data_map_) // for each logged object
    {
      data_name = enumToString(m1.first);
      dataset_name = group_name_ + "/" + data_name;
      num_rows = m1.second.size();
      num_cols = m1.second.at(0).size();
      dims[0] = num_rows;
      dims[1] = num_cols;
      dataspace = H5::DataSpace(2, dims);
      dataset = file.createDataSet(dataset_name, H5::PredType::NATIVE_DOUBLE, dataspace);
      
      double vals[num_rows][num_cols];
      
      for (auto const &m2 : m1.second) // for each dimension in logged object
      {	
	// write each value to its place in the array
	counter = 0;
	for (it = m2.second.begin(); it < m2.second.end(); it++, counter++)
	  vals[m2.first][counter] = *it;
      }
      
      // TODO would be a good idea to do exception handling on this
      dataset.write(vals, H5::PredType::NATIVE_DOUBLE);
    }

    // log data from VectorMap
    for (auto const &m1 : vector_map_) // for each logged object
    {
      data_name = enumToString(m1.first);
      dataset_name = group_name_ + "/" + data_name;
      num_rows = 1.0;
      num_cols = m1.second.size();
      dims[0] = num_rows;
      dims[1] = num_cols;
      dataspace = H5::DataSpace(2, dims);
      dataset = file.createDataSet(dataset_name, H5::PredType::NATIVE_DOUBLE, dataspace);
      
      double vals[num_rows][num_cols];
      
      // write each value to its place in the array
      counter = 0;
      for (it = m1.second.begin(); it < m1.second.end(); it++, counter++)
	vals[0][counter] = *it;
      
      // TODO would be a good idea to do exception handling on this
      dataset.write(vals, H5::PredType::NATIVE_DOUBLE);
    }

    // log attributes
    H5std_string attr_name;
    // log string attributes
    for (auto const &m : str_attr_map_)
    {
      attr_name = enumToString(m.first);
      H5::StrType str_type(0, H5T_VARIABLE);
      H5::DataSpace attr_space(H5S_SCALAR);
      H5::Attribute attr = group.createAttribute(attr_name, str_type, attr_space);
      attr.write(str_type, m.second);
    }
    // log double attributes
    for (auto const &m : dbl_attr_map_)
    {
      attr_name = enumToString(m.first);
      H5::DataSpace attr_space(H5S_SCALAR);
      H5::Attribute attr = group.createAttribute(attr_name, H5::PredType::NATIVE_DOUBLE, attr_space);
      attr.write(H5::PredType::NATIVE_DOUBLE, &m.second);
    }
  }
  else
  {
    std::cout << "\nNo data was logged. Not saving to disk.\n" << std::endl;
  }
}

void TrajectoryLogger::clearData()
{
  for (auto const &m1 : data_map_) // for each logged object
  {
    for (auto const &m2 : m1.second) // for each dimension in logged object
    {
      // create a new vector to discard any saved data
      std::vector<double> log_vector;
      data_map_[m1.first][m2.first] = log_vector;
    }
  }
  data_logged_ = false;
  std::cout <<"\nLogged data cleared. Logging map reset.\n" << std::endl;
}

void TrajectoryLogger::setupDatabase(H5::H5File &file, H5::Group &group)
{
  H5std_string h5_db_ = H5std_string(db_path_ + "/" + db_name_ + ".h5");
  
  boost::filesystem::create_directories(db_path_);
  if (!boost::filesystem::exists(h5_db_))
    file = H5::H5File(h5_db_, H5F_ACC_TRUNC);
  else
    file = H5::H5File(h5_db_, H5F_ACC_RDWR);

  group_name_ = "/trajectory_" + getNextIndexValue();
  group = H5::Group(file.createGroup(group_name_));
  group_name_ += "/raw_data";
  group = H5::Group(file.createGroup(group_name_));

  std::cout << "\nSaving trajectory data to:\n    Database: " << h5_db_
            << "\n    Group: " << group_name_ << std::endl << std::endl;
}

std::string TrajectoryLogger::getNextIndexValue()
{
  int index;
  std::fstream file;
  
  if (!boost::filesystem::exists(index_file_))
  {
    // create file and write first index to it
    file.open(index_file_, std::fstream::out);
    file << 2;
    file.close();
    index = 1;
  }
  else
  {
    // open file, read value for return, increment, write to disk
    file.open(index_file_, std::fstream::in);
    file >> index;
    file.close();
    file.open(index_file_, std::fstream::out | std::ofstream::trunc);
    file << index + 1;
    file.close();
  }
  return std::to_string(index);
}

std::string TrajectoryLogger::enumToString(DataKey key)
{
  switch(key)
  {
    case X            : return "x";
    case X_FT         : return "x_ft";
    case X_DES        : return "x_des";
    case X_ERR        : return "x_err";
    case X_DOT        : return "x_dot";
    case X_DOT_DES    : return "x_dot_des";
    case X_DOT_ERR    : return "x_dot_err";
    case X_DOT_DOT    : return "x_dot_dot";
    case Q            : return "q";
    case Q_DES        : return "q_des";
    case Q_ERR        : return "q_err";
    case Q_DOT        : return "q_dot";
    case Q_DOT_DES    : return "q_dot_des";
    case Q_DOT_ERR    : return "q_dot_err";
    case W_DES        : return "w_des";
    case W_DES_CF     : return "w_des_cf";
    case W_DES_BASE   : return "w_des_base";
    case W_DES_TOOL   : return "w_des_tool";
    case W_RAW        : return "w_raw";
    case W_RAW_BASE   : return "w_raw_base";
    case W_RAW_TOOL   : return "w_raw_tool";
    case W_RAW_CF     : return "w_raw_cf";
    case W_FILT       : return "w_filt";
    case W_FILT_BASE  : return "w_filt_base";
    case W_FILT_TOOL  : return "w_filt_tool";
    case W_FILT_CF    : return "w_filt_cf";
    case W_ERR        : return "w_err";
    case CONSTRAINT   : return "constraints";
    case TAU          : return "tau";
    case TIME         : return "time";
    case SEA_SPRING   : return "sea_spring";
    case SEA_CROSS    : return "sea_cross";
    case SEA_GRAVITY  : return "sea_gravity";
    case SEA_GRAVITY_MODEL : return "sea_gravity_model";
    case GRAVITY      : return "gravity";
    default           : return "[Unknown DataKey]";
  }
}

std::string TrajectoryLogger::enumToString(AttributeKey key)
{
  switch(key)
  {
    case START            : return "start";
    case END              : return "end";
    case RATE             : return "rate";
    case ROBOT_NAME       : return "robot_name";
    case K_X_P            : return "k_x_p";      
    case K_X_D            : return "k_x_d";      
    case K_F_P            : return "k_f_p";  
    case K_F_I            : return "k_f_i";      
    case K_F_V            : return "k_f_v";   
    case K_NULL_P         : return "k_null_p"; 
    case K_NULL_D         : return "k_null_d";  
    case INTEGRAL_DECAY   : return "integral_decay";
    case INTEGRAL_SCALING : return "integral_scaling";
    default   : return "[Unknown AttributeKey]";
  }
}

