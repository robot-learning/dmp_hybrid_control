#include "demonstration_logger.h"
#include <kdl_conversions/kdl_msg.h>


bool DemonstrationLogger::init(ros::NodeHandle &nh)
{
  ROS_INFO("[DemonstrationLogger] Initializing...");

  nh.getParam("use_ft_sensor", use_ft_sensor_);
  nh.getParam("jnt_state_topic", jnt_state_topic_);
  nh.getParam("sea_jnt_state_topic", sea_jnt_state_topic_);
  nh.getParam("root_link", root_link_);
  nh.getParam("tip_link", tip_link_);
  nh.getParam("run_rate", rate_val_);
  nh.getParam("/robot_description", robot_description_);
  nh.getParam("/robot_name", robot_name_);
  nh.param<std::string>("/db_path", db_path_, "/.ros/ll4ma_robot_control");
  db_path_ = std::string(std::getenv("HOME")) + "/" + db_path_;
  nh.param<std::string>("/db_name", db_name_, "trajectories");

  first_log_ = true;
  last_log_ = false;
  do_logging_ = false;
      
  jnt_state_sub_ = nh.subscribe(jnt_state_topic_, 1, &DemonstrationLogger::jointStateCallback, this);
  btn_state_sub_ = nh.subscribe("button_state", 1, &DemonstrationLogger::buttonStateCallback, this);
  sea_jnt_state_sub_ = nh.subscribe(sea_jnt_state_topic_, 1,
				    &DemonstrationLogger::seaJointStateCallback, this);
    
  clear_data_srv_ = nh.advertiseService("/clear_data", &DemonstrationLogger::clearData, this);
  save_data_srv_ = nh.advertiseService("/save_data", &DemonstrationLogger::saveData, this);

  // Initialize the KDL chain
  if (!kdl_parser::treeFromString(robot_description_, robot_tree_))
  {
    ROS_ERROR("[DemonstrationLogger] Failed to construct kdl tree.");
    return false;
  }
  if(!robot_tree_.getChain(root_link_, tip_link_, kdl_chain_))
  {
    ROS_ERROR("[DemonstrationLogger] Failed to construct kdl chain from %s to %s",
	      root_link_.c_str(), tip_link_.c_str());
    return false;
  }

  num_jnts_ = kdl_chain_.getNrOfJoints();
  q_qdot_.resize(num_jnts_);
  fk_solver_.reset(new KDL::ChainFkSolverVel_recursive(kdl_chain_));
  jac_solver_.reset(new KDL::ChainJntToJacSolver(kdl_chain_));
  jac_dot_solver_.reset(new KDL::ChainJntToJacDotSolver(kdl_chain_));
  // dyn_model_solver_.reset(new KDL::ChainDynParam(kdl_chain_, gravity_));

  jdot_qdot_kdl_ = KDL::Twist::Zero();
  J_kdl_.resize(num_jnts_);
  // G_kdl_.resize(num_jnts_);
  
  J_.setZero(6, num_jnts_);
  jdot_qdot_.setZero(6);
  q_dotdot_.setZero(num_jnts_);
  x_dotdot_.setZero(6);
  // G_.setZero(num_jnts_);
  tau_g_.setZero(num_jnts_);

  // Logger
  logger_ = TrajectoryLogger(db_path_, db_name_, nh);
  logger_.registerElement(X, X_DOT, x_xdot_);
  logger_.registerElement(Q, Q_DOT, q_qdot_);
  logger_.registerElement(TIME, time_sec_);
  logger_.setAttribute(RATE, rate_val_);
  logger_.setAttribute(ROBOT_NAME, robot_name_);

  if (use_ft_sensor_)
  {
    nh.getParam("ft_link", ft_link_);
    nh.getParam("transform_wrench", transform_wrench_);
    nh.getParam("ft_topic", ft_topic_);
    nh.getParam("filtered_ft_topic", filtered_ft_topic_);

    // find the index of the force sensor
    for (int i=0; i<kdl_chain_.getNrOfSegments(); i++)
      if (kdl_chain_.getSegment(i).getName() == ft_link_)
	ft_index_ = i;
    ft_index_ += 1; // chain iteration not inclusive, so add one to include FT link

    wrench_state_sub_ = nh.subscribe(ft_topic_, 1, &DemonstrationLogger::wrenchStateCallback, this);
    filtered_wrench_sub_ = nh.subscribe(filtered_ft_topic_, 1,
					&DemonstrationLogger::filteredWrenchStateCallback, this);

    // KDL
    w_raw_ = KDL::Wrench::Zero();
    w_raw_base_ = KDL::Wrench::Zero();
    w_filt_ = KDL::Wrench::Zero();
    w_filt_base_ = KDL::Wrench::Zero();

    logger_.registerElement(W_RAW, w_raw_);
    logger_.registerElement(W_RAW_BASE, w_raw_base_);
    logger_.registerElement(W_FILT, w_filt_);
    logger_.registerElement(W_FILT_BASE, w_filt_base_);
  }
  
  ROS_INFO("[DemonstrationLogger] Complete.");
}

void DemonstrationLogger::run()
{
  ROS_INFO("[DemonstrationLogger] Waiting for robot joint state...");
  ros::spinOnce();
  while (ros::ok() && !jnt_state_received_)
  {
    ros::spinOnce();
    rate_.sleep();
  }
  ROS_INFO("[DemonstrationLogger] Joint state received.");

  if (use_ft_sensor_)
  {
    ROS_INFO("[DemonstrationLogger] Waiting for wrench state...");
    while (ros::ok() && !wrench_state_received_)
    {
      ros::spinOnce();
      rate_.sleep();
    }
    ROS_INFO("[DemonstrationLogger] Wrench state received.");
  }

  ROS_INFO("[DemonstrationLogger] Ready to log demonstrations.");
  while(ros::ok())
  {
    ros::spinOnce();
    if (do_logging_)
    {
      if (first_log_)
      {
	ROS_INFO("[DemonstrationLogger] Logging data...");
	first_log_ = false;
	logger_.setAttribute(START, 0.0);
	num_logged_ = 0.0;
	last_log_ = true;
      }

      // logger_.log(X, X_DOT, X_DOT_DOT, x_xdot_xdotdot_);
      logger_.log(X, X_DOT, x_xdot_);
      logger_.log(Q, Q_DOT, q_qdot_);
      time_sec_ = ros::Time::now().toSec();
      logger_.log(TIME, time_sec_);

      if (use_ft_sensor_)
      {
	logger_.log(W_RAW, w_raw_);
	logger_.log(W_RAW_BASE, w_raw_base_);
	logger_.log(W_FILT, w_filt_);
	logger_.log(W_FILT_BASE, w_filt_base_);
      }
      num_logged_ += 1.0;
    }
    else
    {
      if (last_log_)
      {
	first_log_ = true;
	last_log_ = false;
	ROS_INFO("[DemonstrationLogger] Logging stopped.");
	logger_.setAttribute(END, num_logged_);
      }
    }
    
    rate_.sleep();
  }

  ROS_INFO("[DemonstrationLogger] Logging finished.");
  ROS_INFO("[DemonstrationLogger] Saving to disk...");
  logger_.writeToDisk();
  ROS_INFO("[DemonstrationLogger] Data saved. Exiting.");
}


void DemonstrationLogger::buttonStateCallback(std_msgs::Bool btn_msg)
{
  if (!do_logging_)
    do_logging_ = true;
  else
    do_logging_ = false;
}


void DemonstrationLogger::jointStateCallback(sensor_msgs::JointState jnt_msg)
{
  for (int i = 0; i < num_jnts_; i++)
  {
    q_qdot_.q(i) = jnt_msg.position[i];
    q_qdot_.qdot(i) = jnt_msg.velocity[i];
    q_dotdot_[i] = jnt_msg.effort[i];
  }

  fk_solver_->JntToCart(q_qdot_, x_xdot_);
  // jac_solver_->JntToJac(q_qdot_.q, J_kdl_);
  // dyn_model_solver_->JntToGravity(q_qdot_.q, G_kdl_);
  // jac_dot_solver_->JntToJacDot(q_qdot_, jdot_qdot_kdl_);

  // J_ = J_kdl_.data;
  // // G_ = G_kdl_.data;
  // for (int i = 0; i < 6; i++)
  //   jdot_qdot_[i] = jdot_qdot_kdl_[i];

  // x_dotdot_ = jdot_qdot_ + J_ * (q_dotdot_ - tau_g_);
  // for (int i = 0; i < 6; i++)
  //   x_dotdot_kdl_(i) = x_dotdot_[i];

  // ROS_INFO_STREAM("xdotdot:\n" << x_dotdot_);
  // ROS_INFO_STREAM("qdotdot:\n" << q_dotdot_);
  // ROS_INFO_STREAM("tau_g\n" << tau_g_);


  // x_xdot_xdotdot_ = KDL::FrameAcc(x_xdot_.GetFrame(), x_xdot_.GetTwist(), x_dotdot_kdl_);
  
  // tell the wrench state callback it can start logging
  if (!jnt_state_received_)
    jnt_state_received_ = true;
}


void DemonstrationLogger::seaJointStateCallback(baxter_core_msgs::SEAJointState jnt_state)
{
  for (int i = 0; i < num_jnts_; i++)
    tau_g_[i] = jnt_state.gravity_model_effort[i];

  // if (!sea_jnt_state_received_)
  //   sea_jnt_state_received_ = true;
}


void DemonstrationLogger::wrenchStateCallback(geometry_msgs::WrenchStamped wrench_stmp)
{
  // wrench in F/T sensor frame
  tf::wrenchMsgToKDL(wrench_stmp.wrench, w_raw_);
  // negate so it's how much force robot is applying, not how much is applied to robot
  w_raw_ = -w_raw_;

  // TODO need to have separate state so you can do FK to the force sensor frame
  // fk_solver_->JntToCart(q_qdot_, x_xdot_, ft_index);
  if (transform_wrench_ && jnt_state_received_)
    w_raw_base_ = x_xdot_.GetFrame() * w_raw_; // convert to base frame
  
  if (!wrench_state_received_)
    wrench_state_received_ = true;
}


void DemonstrationLogger::filteredWrenchStateCallback(geometry_msgs::WrenchStamped wrench_stmp)
{
  tf::wrenchMsgToKDL(wrench_stmp.wrench, w_filt_);
  // negate so it's force robot is applying instead of force being applied to robot
  w_filt_ = -w_filt_;

  if (transform_wrench_ && jnt_state_received_)
    w_filt_base_ = x_xdot_.GetFrame() * w_filt_; // convert to base frame
}


bool DemonstrationLogger::clearData(std_srvs::Empty::Request &req, std_srvs::Empty::Response &resp)
{
  logger_.clearData();
  return true;
}

bool DemonstrationLogger::saveData(std_srvs::Empty::Request &req, std_srvs::Empty::Response &resp)
{
  logger_.writeToDisk();
  logger_.clearData();
  return true;
}



int main(int argc, char** argv)
{
  ros::init(argc, argv, "demonstration_logger");
  ros::NodeHandle nh(argv[1]); // assuming namespace is passed as arg from launch or CL
  int run_rate;
  nh.getParam("run_rate", run_rate);
  DemonstrationLogger demo_logger(run_rate);
  demo_logger.init(nh);
  demo_logger.run();
}
