#include "os_inv_dyn_control.h"
#include <eigen_conversions/eigen_kdl.h>
#include <eigen_conversions/eigen_msg.h>
#include <kdl_conversions/kdl_msg.h>


bool OpSpaceInvDynController::configureHook()
{
  ROS_INFO("[OSInvDynControl] Initializing...");

  // call base controller initialization
  if (!OpSpaceController::configureHook())
  {
    ROS_ERROR("[OSInvDynControl] Base instantiation FAILURE");
    return false;
  }

  // Get parameters from from parameter server
  this->getGainsFromParamServer("os_inv_dyn/x_p_gains", nh_, Kp_);
  this->getGainsFromParamServer("os_inv_dyn/x_d_gains", nh_, Kd_);
  this->getGainsFromParamServer("os_inv_dyn/null_p_gains", nh_, Kp_null_);
  this->getGainsFromParamServer("os_inv_dyn/null_d_gains", nh_, Kd_null_);

  // Get joint midpoints for nullspace resolution
  std::vector<double> joint_midpoints;
  nh_.getParam("joint_midpoints", joint_midpoints);
  for (int i = 0; i < num_jnts_; i++)
    q_null_des_[i] = joint_midpoints[i];
  
  ROS_INFO("[OSInvDynControl] Initialization complete.");
  return true;
}

bool OpSpaceInvDynController::startHook()
{
  ROS_INFO("[OSInvDynControl] Waiting for robot joint state...");
  ros::spinOnce();
  while (ros::ok() && !jnt_state_received_)
  {
    ros::spinOnce();
    rate_.sleep();
  }

  if (use_ft_sensor_)
  {
    ROS_INFO("[OSInvDynControl] Waiting for wrench state...");
    while (ros::ok() && !wrench_state_received_)
    {
      ros::spinOnce();
      rate_.sleep();
    }
  }

  ROS_INFO("[OSInvDynControl] Robot state received.");

  this->setCurrentConfigAsDesired();
  
  ROS_INFO("[OSInvDynControl] Control loop is running...");
  ros::spinOnce();

  prev_time_ = ros::Time::now();
  
  while(ros::ok())
  {
    this->updateHook();
    ros::spinOnce();
    rate_.sleep();
  }

  ROS_INFO("[OSInvDynControl] Control loop complete.");
  this->stopHook();
  return true;
}

void OpSpaceInvDynController::updateHook()
{
  time_ = ros::Time::now();
  dt_ = time_ - prev_time_;
  
  // Get current dynamics control components
  jac_solver_->JntToJac(q_qdot_kdl_.q, J_kdl_);
  jac_dot_solver_->JntToJacDot(q_qdot_kdl_, jdot_qdot_kdl_);
  dyn_model_solver_->JntToMass(q_qdot_kdl_.q, Mq_kdl_);
  dyn_model_solver_->JntToGravity(q_qdot_kdl_.q, G_kdl_);
  dyn_model_solver_->JntToCoriolis(q_qdot_kdl_.q, q_qdot_kdl_.qdot, C_kdl_);

  // Get current pose/twist
  fk_solver_->JntToCart(q_qdot_kdl_, x_xdot_kdl_);
  x_kdl_ = x_xdot_kdl_.GetFrame();
  xdot_kdl_ = x_xdot_kdl_.GetTwist();
  // Get transform for FT sensor frame
  fk_solver_->JntToCart(q_qdot_kdl_, x_xdot_kdl_, ft_index_);
  x_ft_kdl_ = x_xdot_kdl_.GetFrame();
  
  // pose and twist error
  x_err_kdl_ = KDL::diff(x_kdl_, x_des_kdl_);
  xdot_err_kdl_ = KDL::diff(xdot_kdl_, xdot_des_kdl_);

  // KDL to Eigen conversions
  J_ = J_kdl_.data;
  Mq_ = Mq_kdl_.data;
  G_ = G_kdl_.data;
  C_ = C_kdl_.data;
  for (int i = 0; i < 6; i++)
  {
    Jdot_qdot_[i] = jdot_qdot_kdl_[i];
    x_err_[i] = x_err_kdl_[i];
    xdot_err_[i] = xdot_err_kdl_[i];
  }
  
  /* ======================================== Control Law ======================================== */

  // task space inertia
  this->getPseudoInverse(J_ * Mq_.inverse() * J_.transpose(), Mx_, 1.e-5);
  
  // task space control signal
  ux_ = Kp_ * x_err_ + Kd_ * xdot_err_ + xdotdot_des_ - Jdot_qdot_;
  
  // joint space control signal
  tau_ = J_.transpose() * Mx_ * ux_ + C_;

  if (compensate_gravity_)
    robot_interface_->compensateGravity(tau_, G_);

  // null space control signal
  if (use_posture_)
  {
    tau_posture_ = Kp_null_ * (q_null_des_ - q_) - Kd_null_ * qdot_;
    nullSpaceProjection(tau_, tau_posture_, J_, Mx_, Mq_);
  }
  
  /* ============================================================================================= */  
  
  robot_interface_->publishTorqueCommand(tau_);
  
  // publish current robot state
  this->publishCurrentRobotState();
  
  if (log_)
  {
    logger_.log(X, x_kdl_);
    logger_.log(X_FT, x_ft_kdl_);
    logger_.log(X_DES, x_des_kdl_);
    logger_.log(X_ERR, x_err_);
    logger_.log(X_DOT, xdot_kdl_);
    logger_.log(X_DOT_DES, xdot_des_kdl_);
    logger_.log(X_DOT_ERR, xdot_err_);
    logger_.log(Q, q_);
    logger_.log(Q_DOT, qdot_);
    logger_.log(W_RAW, w_raw_);
    logger_.log(W_FILT, w_filt_);
  }

  prev_time_ = time_;
}

void OpSpaceInvDynController::stopHook()
{
  if (log_)
  {
    ROS_INFO("[OSInvDynControl] Writing trajectory data to disk...");
    logger_.writeToDisk();
    ROS_INFO("[OSInvDynControl] Trajectory data saved. Exiting");
  }
}

void OpSpaceInvDynController::cleanupHook()
{
  // nothing to do here for now.
}
