#include "os_parallel_force_position_control.h"
#include <eigen_conversions/eigen_kdl.h>
#include <eigen_conversions/eigen_msg.h>
#include <kdl_conversions/kdl_msg.h>


bool ParallelForcePositionController::init(ros::NodeHandle &nh)
{
  // call base controller initialization
  bool status = OpSpaceController::init(nh);
  if (!status)
  {
    ROS_ERROR_STREAM("[Parallel Force/Position] Base instantiation FAILURE");
    return false;
  }

  // initialize all matrices to zeros (avoids gazebo control blowups)
  wrench_ = Eigen::Matrix<double, 6, 1>::Zero();
  prev_wrench_ = Eigen::Matrix<double, 6, 1>::Zero();
  wrench_des_ = Eigen::Matrix<double, 6, 1>::Zero();
  wrench_err_ = Eigen::Matrix<double, 6, 1>::Zero();
  Kf_ = Eigen::Matrix<double, 6, 1>::Zero();
  Ki_ = Eigen::Matrix<double, 6, 1>::Zero();
  
  // get parameters from parameter server
  this->readParamsFromServer("parallel_control/x_p_gains", nh, Kp_);
  this->readParamsFromServer("parallel_control/x_d_gains", nh, Kd_);
  this->readParamsFromServer("parallel_control/null_p_gains", nh, Kp_null_);
  this->readParamsFromServer("parallel_control/null_d_gains", nh, Kd_null_);
  this->readParamsFromServer("parallel_control/f_p_gains", nh, Kf_);
  this->readParamsFromServer("parallel_control/f_i_gains", nh, Ki_);

  // setup force sensor link
  nh.param<std::string>("/ft_link", ft_link_, "ati_link");
  ft_index_ = kdl_.getSegmentIndex(ft_link_);

  // initialize desired configuration to current
  this->setCurrentConfigAsDesired();
      
  // ROS subscribers and publishers
  op_space_cmd_sub_ = nh.subscribe("op_space_cmd", 1,
                                   &ParallelForcePositionController::opSpaceCmdCallback, this);
  wrench_state_sub_ = nh.subscribe(
    "/ft_sensor/wrench", 1, &ParallelForcePositionController::wrenchStateCallback, this);
  joint_cmd_pub_ = nh.advertise<sensor_msgs::JointState>("joint_cmd", 1);

  if (log_)
  {
    logger_.registerVector(W, wrench_);
    logger_.registerVector(W_DES, wrench_des_);
    logger_.registerVector(W_ERR, wrench_err_);
  }

  ROS_INFO_STREAM("[Parallel Force/Position] Initialization complete.");
  return true;
}

void ParallelForcePositionController::update(const ros::Time &time)
{
  dt_ = time - prev_time_;

  // use solvers to get current control components
  fk_solver_->JntToCart(q_qdot_kdl_, x_xdot_kdl_);
  jac_solver_->JntToJac(q_qdot_kdl_.q, J_kdl_);
  jac_dot_solver_->JntToJacDot(q_qdot_kdl_, jdot_qdot_kdl_);
  dyn_model_solver_->JntToMass(q_qdot_kdl_.q, Mq_kdl_);  
  dyn_model_solver_->JntToGravity(q_qdot_kdl_.q, G_kdl_);
  dyn_model_solver_->JntToCoriolis(q_qdot_kdl_.q, q_qdot_kdl_.qdot, C_kdl_);

  // KDL to Eigen conversions
  tf::transformKDLToEigen(x_xdot_kdl_.GetFrame(), x_);
  tf::twistKDLToEigen(x_xdot_kdl_.GetTwist(), xdot_);
  tf::twistKDLToEigen(jdot_qdot_kdl_, Jdot_qdot_);
  J_ = J_kdl_.data;
  Mq_ = Mq_kdl_.data;
  G_ = G_kdl_.data;
  C_ = C_kdl_.data;
  
  // compute pose and wrench error
  this->computeTaskSpaceDifference(x_, x_des_, x_err_);
  xdot_err_ = xdot_des_ - xdot_;
  wrench_err_ = wrench_des_ - wrench_;

  /* ================================================================================== */

  // task space inertia
  this->getPseudoInverse(J_ * Mq_.inverse() * J_.transpose(), Mx_, 1.e-5);

  // task space control signal
  ux_ =
    (Kp_.asDiagonal() * (x_err_
                         - Kf_.asDiagonal() * wrench_err_ // minus b.c. force measured against sensor
                         - Ki_.asDiagonal() * (wrench_ - prev_wrench_) * dt_.toSec()))
    + xdotdot_des_
    + Kd_.asDiagonal() * xdot_err_
    - Jdot_qdot_;

  // joint space control signal
  uq_ = J_.transpose() * ((Mx_ * ux_) + wrench_des_) + G_ + C_;

  // null space control signal
  if (use_posture_)
  {
    uq_null_ = Kp_null_.asDiagonal() * (q_null_des_ - q_) - Kd_null_.asDiagonal() * qdot_;
    uq_ += (I_N_ - J_.transpose() * (Mq_.inverse() * J_.transpose() * Mx_).transpose()) * uq_null_;
  }
 
  /* ================================================================================== */
  
  // populate joint command with efforts and publish message
  for (int i = 0; i < NUM_JOINTS; i++)
    joint_cmd_.effort[i] = uq_[i];
  joint_cmd_pub_.publish(joint_cmd_);
  
  // publish current pose
  tf::poseEigenToMsg(x_, pose_state_);
  pose_state_pub_.publish(pose_state_);

  if (log_)
  {
    // x and x_des are logged in computeTaskSpaceDifference
    logger_.log(X_ERR, x_err_);
    logger_.log(X_DOT, xdot_);
    logger_.log(X_DOT_DES, xdot_des_);
    logger_.log(X_DOT_ERR, xdot_err_);
    logger_.log(Q, q_);
    logger_.log(Q_DOT, qdot_);
    logger_.log(W, wrench_);
    logger_.log(W_DES, wrench_);
    logger_.log(W_ERR, wrench_err_);
  }

  prev_wrench_ = wrench_;
  prev_time_ = time;
}

void ParallelForcePositionController::starting(const ros::Time &time)
{
  ROS_INFO_STREAM("[Parallel Force/Position] Control loop is running.");
  ros::spinOnce();

  prev_time_ = time;
  
  while(ros::ok())
  {
    this->update(ros::Time::now());
    ros::spinOnce();
    rate_.sleep();
  }

  this->stopping(ros::Time::now());
  ROS_INFO_STREAM("[Parallel Force/Position] Control loop complete. Exiting.");
}

void ParallelForcePositionController::stopping(const ros::Time &time)
{
  if (log_)
  {
    ROS_INFO_STREAM("[Parallel Force/Position] Writing trajectory data to disk...");
    logger_.writeToDisk();
    ROS_INFO_STREAM("[Parallel Force/Position] Trajectory data saved. Exiting");
  }
}

void ParallelForcePositionController::opSpaceCmdCallback(ll4ma_robot_control::OpSpaceCommand cmd_msg)
{
  // desired pose
  tf::poseMsgToEigen(cmd_msg.pose, x_des_);

  // desired null posture
  if (cmd_msg.null_posture.size() > 0)
  {
    for (int i = 0; i < cmd_msg.null_posture.size(); i++)
      q_null_des_(i) = cmd_msg.null_posture[i];
  }
  
  // desired twist
  tf::twistMsgToEigen(cmd_msg.twist, xdot_des_);

  // desired wrench
  tf::wrenchMsgToEigen(cmd_msg.wrench, wrench_des_);
}

void ParallelForcePositionController::wrenchStateCallback(geometry_msgs::WrenchStamped state_msg)
{
  // wrench in force/torque sensor frame 
  tf::wrenchMsgToKDL(state_msg.wrench, wrench_kdl_);
  // convert to world frame
  fk_solver_->JntToCart(q_qdot_kdl_, ft_x_xdot_kdl_, ft_index_);
  wrench_kdl_ = ft_x_xdot_kdl_.GetFrame() * wrench_kdl_;
  tf::wrenchKDLToEigen(wrench_kdl_, wrench_);
}
