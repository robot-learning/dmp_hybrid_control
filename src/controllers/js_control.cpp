#include "js_control.h"


namespace controller_interface
{
  bool JointSpaceController::configureHook()
  {
    ROS_INFO("[BaseJSControl] Initializing...");

    if (!BaseController::configureHook())
    {
      ROS_ERROR("[BaseJSControl] Parent controller initialization failed.");
      return false;
    }
    
    if (!nh_.getParam("jnt_des_cmd_topic", jnt_des_cmd_topic_))
    {
      ROS_ERROR("[JSPositionControl] Initialization FAILURE. Desired joint "
		"command topic name not on parameter server.");
      return false;
    }
    
    // initialize all matrices to zeros (avoids gazebo control blowups)
    q_.setZero(num_jnts_);
    q_des_.setZero(num_jnts_);
    q_err_.setZero(num_jnts_);
    q_dot_.setZero(num_jnts_);
    q_dot_des_.setZero(num_jnts_);
    q_dot_err_.setZero(num_jnts_);
    u_.setZero(num_jnts_);
    Kp_.setZero(num_jnts_, num_jnts_);
    Kd_.setZero(num_jnts_, num_jnts_);

    if (log_)
    {
      logger_.registerElement(Q, q_);
      logger_.registerElement(Q_DES, q_des_);
      logger_.registerElement(Q_ERR, q_err_);
      logger_.registerElement(Q_DOT, q_dot_);
      logger_.registerElement(Q_DOT_DES, q_dot_des_);
      logger_.registerElement(Q_DOT_ERR, q_dot_err_);
      logger_.setAttribute(ROBOT_NAME, robot_name_);
      logger_.setAttribute(RATE, rate_val_);
    }

    // ROS
    jnt_state_sub_ = nh_.subscribe(jnt_state_topic_, 1, &JointSpaceController::jointStateCallback,
				   this);
    jnt_cmd_sub_ = nh_.subscribe(jnt_des_cmd_topic_, 1, &JointSpaceController::jointCmdCallback,
				 this);

    ROS_INFO("[BaseJSControl] Initialization complete.");
    return true;
  }

  bool JointSpaceController::getStopControl()
  {
    return stop_control_;
  }

  void JointSpaceController::setStopControl(bool status)
  {
    stop_control_ = status;
  }

  void JointSpaceController::setCurrentConfigAsDesired()
  {
    ROS_INFO("[BaseJSControl] Setting current configuration as initial setpoint.");
    for (int i = 0; i < num_jnts_; i++)
      q_des_[i] = q_[i];
  }

  void JointSpaceController::jointStateCallback(sensor_msgs::JointState state_msg)
  {
    // set current as desired if this is the first state received
    if (!jnt_state_received_)
    {
      for (int i = 0; i < num_jnts_; i++)
	q_des_[i] = state_msg.position[i];
      jnt_state_received_ = true;
    }
  
    // save the current state
    for (int i = 0; i < num_jnts_; i++)
    {
      q_[i] = state_msg.position[i];
      q_dot_[i] = state_msg.velocity[i];
    }
  }

  void JointSpaceController::jointCmdCallback(sensor_msgs::JointState cmd_msg)
  {
    // for now assuming only positions are commanded, can add others if needed
    for (int i = 0; i < num_jnts_; i++)
      q_des_[i] = cmd_msg.position[i];
  }
  
} // namespace controller_interface
