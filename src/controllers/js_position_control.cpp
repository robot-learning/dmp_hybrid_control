#include "js_position_control.h"


namespace controller_interface
{

  bool JointSpacePositionController::configureHook()
  {
    // Call parent initialization
    if (!JointSpaceController::configureHook())
    {
      ROS_ERROR("[JSPositionControl] Base initialization FAILURE");
      return false;
    }
    
    // Get controller params from param server
    this->getGainsFromParamServer("js_position_control/p_gains", nh_, Kp_);
    this->getGainsFromParamServer("js_position_control/d_gains", nh_, Kd_);
            
    prev_time_ = ros::Time::now();
    time_ = ros::Time::now();
    
    ROS_INFO("[JSPositionControl] Initialization complete.");
    return true;
  }
  
  bool JointSpacePositionController::startHook()
  {
    ROS_INFO("[JSPositionControl] Waiting for robot state...");
    ros::spinOnce();
    while (ros::ok() && !jnt_state_received_)
    {
      ros::spinOnce();
      rate_.sleep();
    }
    ROS_INFO("[JSPositionControl] Robot state received.");

    this->setCurrentConfigAsDesired();
    
    ROS_INFO("[JSPositionControl] Control loop is running...");
    ros::spinOnce();
    
    prev_time_ = ros::Time::now();
    stop_control_ = false;

    while(ros::ok() && !stop_control_)
    {
      this->updateHook();
      ros::spinOnce();
      rate_.sleep();
    }
    
    ROS_INFO("[JSPositionControl] Control loop complete.");
    this->stopHook();
    return true;
  }
  
  void JointSpacePositionController::updateHook()
  {
    time_ = ros::Time::now();
    dt_ = time_ - prev_time_;
    
    // Eigen to KDL conversions
    for (int i = 0; i < num_jnts_; i++)
    { 
      q_qdot_kdl_.q(i) = q_[i];
      q_qdot_kdl_.qdot(i) = q_dot_[i];
    }
    
    // gravity and coriolis
    dyn_model_solver_->JntToGravity(q_qdot_kdl_.q, G_kdl_);
    dyn_model_solver_->JntToCoriolis(q_qdot_kdl_.q, q_qdot_kdl_.qdot, C_kdl_);
    G_ = G_kdl_.data;
    C_ = C_kdl_.data;
    
    // joint position and velocity error
    q_err_ = q_des_ - q_;
    q_dot_err_ = q_dot_des_ - q_dot_;
    
    /* =========================================================================================== */

    u_ = Kp_ * q_err_ + Kd_ * q_dot_err_;
    
    if (compensate_gravity_)
      robot_interface_->compensateGravity(u_, G_);	
    
    /* =========================================================================================== */

    robot_interface_->publishTorqueCommand(u_);

    if (log_)
    {
      logger_.log(Q, q_);
      logger_.log(Q_DES, q_des_);
      logger_.log(Q_ERR, q_err_);
      logger_.log(Q_DOT, q_dot_);
      logger_.log(Q_DOT_DES, q_dot_des_);
      logger_.log(Q_DOT_ERR, q_dot_err_);
    }
    
    prev_time_ = time_;
  }
  
  void JointSpacePositionController::stopHook()
  {
    if (log_)
    {
      // ROS_INFO("[JSPositionControl] Writing trajectory data to disk...");
      // logger_.writeToDisk();
      // ROS_INFO("[JSPositionControl] Trajectory data saved. Exiting");
    }
  }
  
  void JointSpacePositionController::cleanupHook()
  {
    // nothing to do here for now
  }  
}
