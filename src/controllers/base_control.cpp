#include "base_control.h"
#include "lbr4_interface.h"
#include "baxter_interface.h"


namespace controller_interface
{
  bool BaseController::configureHook()
  {
    ROS_INFO("[BaseControl] Initializing...");

    bool success = true;
    // Global params
    success &= nh_.getParam("/robot_name", robot_name_);
    success &= nh_.getParam("/sim", is_sim_);
    success &= nh_.getParam("/robot_description", robot_description_);
    // Local params
    success &= nh_.getParam("compensate_gravity", compensate_gravity_);
    success &= nh_.getParam("jnt_state_topic", jnt_state_topic_);
    success &= nh_.getParam("root_link", root_link_);
    success &= nh_.getParam("tip_link", tip_link_);

    if (!success)
    {
      ROS_ERROR("[BaseControl] Failed to load params from param server.");
      return false;
    }

    nh_.param<bool>("/log", log_, false);
    nh_.param<std::string>("/db_path", db_path_, "/.ros/ll4ma_robot_control");
    nh_.param<std::string>("/db_name", db_name_, "trajectories");

    db_path_ = std::string(std::getenv("HOME")) + "/" + db_path_;

    // Initialize the KDL chain
    if (!kdl_parser::treeFromString(robot_description_, robot_tree_))
    {
      ROS_ERROR("[BaseControl] Failed to construct kdl tree.");
      return false;
    }

    if(!robot_tree_.getChain(root_link_, tip_link_, kdl_chain_))
    {
      ROS_ERROR("[BaseControl] Failed to construct kdl chain from %s to %s",
		root_link_.c_str(), tip_link_.c_str());
      return false;
    }

    num_jnts_ = kdl_chain_.getNrOfJoints();
    for (int i = 0; i < kdl_chain_.getNrOfSegments(); i++)
      if (kdl_chain_.getSegment(i).getJoint().getType() != KDL::Joint::JointType::None)
	jnt_names_.push_back(kdl_chain_.getSegment(i).getJoint().getName());

    // Setup robot interface
    if (robot_name_ == "baxter")
      robot_interface_ = new robot_interface::BaxterInterface(nh_);
    else if (robot_name_ == "lbr4")
      robot_interface_ = new robot_interface::LBR4Interface(nh_);
    else
    {
      ROS_ERROR_STREAM("[BaseControl] Unknown robot: " << robot_name_);
      ROS_ERROR("[BaseControl] Could not initialize robot interface.");
      return false;
    }
    robot_interface_->init(jnt_names_);

    G_.setZero(num_jnts_);
    C_.setZero(num_jnts_);
    q_qdot_kdl_.resize(num_jnts_);
    G_kdl_.resize(num_jnts_);
    C_kdl_.resize(num_jnts_);
    
    dyn_model_solver_.reset(new KDL::ChainDynParam(kdl_chain_, gravity_));

    if (log_)
      logger_ = TrajectoryLogger(db_path_, db_name_, nh_);      

    system_ok_ = true;

    ROS_INFO("[BaseControl] Initialization complete.");
    return true;
  }

  void BaseController::initJointState(sensor_msgs::JointState &jnt_state)
  {
    ROS_INFO("[BaseControl] Initializing joint state...");
    jnt_state = sensor_msgs::JointState();
    for (int i=0; i < num_jnts_; ++i)
    {
      jnt_state.name.push_back(jnt_names_[i]);
      jnt_state.position.push_back(0.0);
      jnt_state.velocity.push_back(0.0);
      jnt_state.effort.push_back(0.0);
    }
    ROS_INFO("[BaseControl] Joint state initialization complete.");
  }
  
} // namespace controller_interface
