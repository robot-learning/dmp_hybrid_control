#include "controller_manager.h"

// TODO this is still somewhat hacked. It would be preferable to not have to load all the controllers
// and only load what will be switched to, or if you're going to load them all, then allow arbitrary
// switching between controllers. Currently, only switching from joint position control to any of the
// other op space controllers is implemented.

namespace controller_interface
{
  bool ControllerManager::switchController(ll4ma_robot_control::SwitchControl::Request &req,
                                           ll4ma_robot_control::SwitchControl::Response &res)
  {
    ROS_INFO("[ControllerManager] Switching from joint position control...");
    control_type_ = type_map_[req.control_type];
    js_position_controller_.setStopControl(req.state);
    res.status = js_position_controller_.getStopControl();
    ROS_INFO("[ControllerManager] Control switch complete.");
    return true;
  }

  bool ControllerManager::init()
  {
    ROS_INFO("[ControllerManager] Initializing controllers.");

    bool success = true;
    nh_.getParam("control_type", control_type_str_);
    control_type_ = type_map_[control_type_str_];

    if (switch_control_)
    {
      ROS_INFO("[ControllerManager] Initializing for control switching...");
      success &= js_position_controller_.configureHook();
      switch(control_type_)
      {
      case OS_INV_DYN:
      {
	success &= inv_dyn_controller_.configureHook();
	break;
      }
      case OS_FORCE_NULL_INV_DYN:
      {
	success &= force_null_controller_.configureHook();
	break;
      }
      case HYBRID_FORCE_POSITION:
      {
	success &= hybrid_controller_.configureHook();
	break;
      }
      default:
	ROS_ERROR_STREAM("Unknown control type: " << control_type_);
      }

      srv_ = nh_.advertiseService("switch_controller", &ControllerManager::switchController, this);
    }
    else
    {
      ROS_INFO("[ControllerManager] Initializing for one control mode...");
      switch(control_type_)
      {
      case JS_POSITION:
      {
	success = js_position_controller_.configureHook();
	break;
      }
      case OS_INV_DYN:
      {
	success = inv_dyn_controller_.configureHook();
	break;
      }
      case OS_FORCE_NULL_INV_DYN:
      {
	success = force_null_controller_.configureHook();
	break;
      }
      case HYBRID_FORCE_POSITION:
      {
	success = hybrid_controller_.configureHook();
	break;
      }
      default:
        ROS_ERROR_STREAM("Unknown control type: " << control_type_);
      }
    }

    if (success)
      ROS_INFO("[ControllerManager] Initialization complete.");
    else
      ROS_ERROR("[ControllerManager] Initialization FAILURE.");

    return success;
  }

  void ControllerManager::run()
  {
    if (switch_control_)
    {
      js_position_controller_.startHook();
      while(!js_position_controller_.getStopControl())
      {
	ros::spinOnce();
	rate_.sleep();
      }
      
      switch(control_type_)
      {
      case OS_INV_DYN:
      {
	inv_dyn_controller_.setCurrentConfigAsDesired();
	js_position_controller_.stopHook();
	inv_dyn_controller_.startHook();
	break;
      }
      case OS_FORCE_NULL_INV_DYN:
      {
        force_null_controller_.setCurrentConfigAsDesired();
        js_position_controller_.stopHook();
        force_null_controller_.startHook();
        break; 
      }
      case PARALLEL_FORCE_POSITION:
      {
        ROS_WARN("Parallel Force/Position control is not ready yet.");
        break; 
      }
      case HYBRID_FORCE_POSITION:
      {
	hybrid_controller_.setCurrentConfigAsDesired();
	js_position_controller_.stopHook();
	hybrid_controller_.startHook();
	break;
      }
      default:
        ROS_ERROR_STREAM("Unknown control type: " << control_type_);
      }
    }
    else
    {
      ROS_INFO("[ControllerManager] Starting the controller...");
      switch(control_type_)
      {
      case JS_POSITION:
      {
	js_position_controller_.startHook();
	break;
      }
      case OS_INV_DYN:
      {
	inv_dyn_controller_.startHook();
	break;
      }
      case OS_FORCE_NULL_INV_DYN:
      {
	force_null_controller_.startHook();
	break;
      }
      case HYBRID_FORCE_POSITION:
      {
	hybrid_controller_.startHook();
	break;
      }
      default:
        ROS_ERROR_STREAM("Unknown control type: " << control_type_);
      }
    }
  }
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "ll4ma_controller_manager");
  ros::NodeHandle nh(argv[1]); // assuming namespace is passed as arg from launch or CL
  int run_rate;
  bool switch_control;
  nh.getParam("run_rate", run_rate);
  nh.getParam("switch_control", switch_control);
  controller_interface::ControllerManager manager(nh, run_rate, switch_control);
  bool success = manager.init();
  if (success)
    manager.run();
}
