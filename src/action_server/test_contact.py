#!/usr/bin/env python
import rospy
import numpy as np
from std_msgs.msg import Bool
from geometry_msgs.msg import WrenchStamped


if __name__ == '__main__':
    rospy.init_node('test_contact')
    window_size = 50
    contact_pub = rospy.Publisher("/in_contact", Bool, queue_size=1)
    mu_noise = 0.53836
    rate = rospy.Rate(100)
    force_windows = []
    cached_value = False

    for i in range(6):
        force_windows.append([0.0] * window_size)


    def in_contact():
        global cached_value
        try:
            mu_window = abs(np.mean(np.linalg.norm(force_windows, axis=0)))
        except (AttributeError):
            rospy.logwarn("AttributeError: conjugate. Returning cached value: %r" % cached_value)
            return cached_value
        in_contact = mu_window > mu_noise
        contact_pub.publish(in_contact)
        cached_value = in_contact
        return in_contact

    def wrench_cb(msg):        
        for window in force_windows:
            window.pop(0)
        # add the new values
        force_windows[0].append(msg.wrench.force.x)
        force_windows[1].append(msg.wrench.force.y)
        force_windows[2].append(msg.wrench.force.z)
        force_windows[3].append(msg.wrench.torque.x)
        force_windows[4].append(msg.wrench.torque.y)
        force_windows[5].append(msg.wrench.torque.z)

    wrench_sub = rospy.Subscriber("/ethdaq_data", WrenchStamped, wrench_cb)

    print "Publishing contacts..."
    while not rospy.is_shutdown():
        in_contact()
        rate.sleep()
    print "Done."
