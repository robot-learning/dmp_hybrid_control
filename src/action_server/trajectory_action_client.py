import sys
import roslib
import rospy
import actionlib
from trajectory_util import (
    _POSES,
    _TWISTS,
    _ACCELS,
    _WRENCHES,
    _CONSTRAINTS,
    _CONSTRAINT_FRAMES,
    _JOINT_POSITIONS
)
from geometry_msgs.msg import (
    Pose,
    Point,
    Quaternion,
    Twist,
    Wrench,
    Vector3
)
from sensor_msgs.msg import JointState
from ll4ma_robot_control.msg import (
    OpSpaceTrajectoryAction,
    OpSpaceTrajectoryGoal,
    Constraint
)
from ll4ma_policy_learning.msg import DMPConfiguration, DMPWeightVector


class TrajectoryActionClient:

    def __init__(self, rospy_init=True):
        if rospy_init:
            rospy.init_node("trajectory_action_client")
        self.robot_name = rospy.get_param("/robot_name", "lbr4")
        self.use_constraint_frame = rospy.get_param("/%s/use_constraint_frame"
                                                    % self.robot_name, False)
        self.ns = "/%s/execute_trajectory" % self.robot_name
        self.client = actionlib.SimpleActionClient(self.ns, OpSpaceTrajectoryAction)
        rospy.loginfo("[TrajectoryActionClient] Trying to connect with action server...")
        server_running = self.client.wait_for_server(timeout=rospy.Duration(10.0))
        if server_running:
            rospy.loginfo("[TrajectoryActionClient] Connected!")
        else:
            rospy.logwarn("[TrajectoryActionClient] You're probably connected to action server.")
            # TODO make a better check if this is important, for some reason when this is launched
            # with Gazebo it says it times out waiting, but does so immediately without actually
            # waiting, even though it is in fact connected. I think ROS is making more of a check
            # than just connection, so look into ROS source code for action server to check.

    def set_trajectory_goal(self, data, phase=None):
        rospy.loginfo("[TrajectoryActionClient] Setting trajectory goal...")
        self.goal = OpSpaceTrajectoryGoal()
        trajectory = self._get_trajectory(data, phase)
        if trajectory is None or not trajectory[_POSES]:
            rospy.logerr("[TrajectoryActionClient] Trajectory is empty. Exiting.")
            return False
        
        # populate goal
        for i, pose in enumerate(trajectory[_POSES]):
            # add poses
            self.goal.trajectory.poses.append(pose)
            # add other components if available
            if _TWISTS in trajectory.keys() and len(trajectory[_TWISTS]) > i:
                self.goal.trajectory.twists.append(trajectory[_TWISTS][i])
            if _ACCELS in trajectory.keys() and len(trajectory[_ACCELS]) > i:
                self.goal.trajectory.accels.append(trajectory[_ACCELS][i])
            if _WRENCHES in trajectory.keys() and len(trajectory[_WRENCHES]) > i:
                self.goal.trajectory.wrenches.append(trajectory[_WRENCHES][i])
            if _CONSTRAINTS in trajectory.keys() and len(trajectory[_CONSTRAINTS]) > i:
                self.goal.trajectory.constraints.append(trajectory[_CONSTRAINTS][i])
            if _CONSTRAINT_FRAMES in trajectory.keys() and len(trajectory[_CONSTRAINT_FRAMES]) > i:
                self.goal.trajectory.constraint_frames.append(trajectory[_CONSTRAINT_FRAMES][i])
                            
        rospy.loginfo("[TrajectoryActionClient] Goal set.")
        return True

    def set_joint_trajectory_goal(self, data):
        rospy.loginfo("[TrajectoryActionClient] Setting joint trajectory goal...")
        self.goal = OpSpaceTrajectoryGoal()
        trajectory = self._get_joint_trajectory(data)
        # populate goal
        for i, joint_position in enumerate(trajectory[_JOINT_POSITIONS]):
            js = JointState()
            js.position = joint_position
            self.goal.trajectory.joint_states.append(js)
        rospy.loginfo("[TrajectoryActionClient] Joint goal set.")
        return True

    def set_dmp_goal(self, trajectory, phase_params, phase_attrs, phase_types):
        rospy.loginfo("[TrajectoryActionClient] Setting DMP goal...")
        self.goal = OpSpaceTrajectoryGoal()
        self.goal.trajectory.dmp_configs = self._get_dmp_configs(phase_params, phase_attrs, phase_types)
        self.goal.trajectory.commanded_trajectory = trajectory
        rospy.loginfo("[TrajectoryActionClient] DMP goal set.")
        return True
        
    def send_goal(self, timeout=1000.0):
        rospy.loginfo("[TrajectoryActionClient] Sending goal to %s action server..." % self.ns)
        self.goal.trajectory.header.stamp = rospy.Time.now()
        self.client.send_goal(self.goal)
        rospy.loginfo("[TrajectoryActionClient] Goal sent successfully.")
        self.wait_for_result(timeout)

    def send_joint_goal(self, timeout=100.0):
        rospy.loginfo("[TrajectoryActionClient] Sending joint goal to %s action server..." % self.ns)
        self.goal.trajectory.header.stamp = rospy.Time.now()
        self.client.send_goal(self.goal)

    def cancel_goal(self):
        self.client.cancel_goal()

    def wait_for_result(self, timeout=1000.0):
        rospy.loginfo("[TrajectoryActionClient] Waiting for result...")
        success = self.client.wait_for_result(timeout=rospy.Duration(timeout))
        if success:
            rospy.loginfo("[TrajectoryActionClient] Result received: %s" % self.get_result())
        else:
            rospy.logwarn("[TrajectoryActionClient] Timed out waiting for result.")

    def get_result(self):
        return self.client.get_result()
        
    def _get_trajectory(self, data, phase=None):
        if phase is not None:
            start = int(data[phase].attrs['start'])
            end = int(data[phase].attrs['end'])
            phase_type = data[phase].attrs['type']
            rospy.loginfo("[TrajectoryActionClient] Generating trajectory for %s." % phase)
        else:
            start = 0
            end = data['raw_data']['x'].shape[1]
            phase_type = 'full'
            rospy.loginfo("[TrajectoryActionClient] Generating full trajectory.")
        
        if phase_type not in ['making_contact', 'in_contact', 'breaking_contact', 'full']:
            rospy.logerr("[TrajectoryActionClient] Unknown phase type: %s" % phase_type)
            return None

        trajectory = {}
        trajectory[_POSES] = []
        trajectory[_TWISTS] = []
        trajectory[_ACCELS] = []
        trajectory[_WRENCHES] = []
        trajectory[_CONSTRAINTS] = []
        trajectory[_CONSTRAINT_FRAMES] = []
        for i in range(start, end):
            p = data['raw_data'][_POSES][:, i]
            trajectory[_POSES].append(Pose(Point(*p[:3]), Quaternion(*p[3:])))
            if _TWISTS in data['raw_data'].keys():
                t = data['raw_data'][_TWISTS][:, i]
                trajectory[_TWISTS].append(Twist(Vector3(*t[:3]), Vector3(*t[3:])))
            if _ACCELS in data['raw_data'].keys():
                a = data['raw_data'][_ACCELS][:, i]
                trajectory[_ACCELS].append(Twist(Vector3(*a[:3]), Vector3(*a[3:])))
            if _WRENCHES in data['raw_data'].keys():
                w = data['raw_data'][_WRENCHES][:, i]
                trajectory[_WRENCHES].append(Wrench(Vector3(*w[:3]), Vector3(*w[3:])))
            if _CONSTRAINT_FRAMES in data['raw_data'].keys():
                cf = data['raw_data'][_CONSTRAINT_FRAMES][:, i]
                trajectory[_CONSTRAINT_FRAMES].append(Pose(Point(),
                                                           Quaternion(cf[0], cf[1], cf[2], cf[3])))

        # handle peculiarities of each phase
        trajectory[_CONSTRAINTS] = self._get_constraints(phase_type, len(trajectory[_POSES]))        
        if phase_type == 'making_contact':
            # set zero desired velocity for last time step
            trajectory[_TWISTS][-1] = Twist()
        if phase_type == 'full':
            # TODO assuming for now phases 1,2,3 correspond in order to make,keep,break contact
            for i in [1, 2, 3]:
                phase = "phase_%d" % i
                start = int(data[phase].attrs['start'])
                end = int(data[phase].attrs['end'])
                length = (end - start)
                phase_type = data[phase].attrs['type']
                constraints = self._get_constraints(phase_type, length)
                for c in constraints:
                    trajectory[_CONSTRAINTS].append(c)
        if len(trajectory[_CONSTRAINTS]) != len(trajectory[_POSES]):
            rospy.logerr("[TrajectoryActionClient] Length of trajectory constraints is not right.")
            rospy.logerr("[TrajectoryActionClient] Expected: %d, Actual: %d"
                         %(len(trajectory[_POSES]), len(trajectory[_CONSTRAINTS])))
            return None
            
        return trajectory

    def _get_constraints(self, phase_type, length):
        constraints = []
        if phase_type == 'making_contact':
            # enable position control constraints
            for i in range(length):
                constraint = Constraint()
                constraint.values = [1, 1, 1, 1, 1, 1]
                constraints.append(constraint)
            # enable force constraint in z-axis for last time step
            constraints[-1].values = [1, 1, 1, 1, 1, 1] # TEMPORARY to avoid enabling force control
        elif phase_type == 'in_contact':
            # enable force constraint in z-axis for all time steps
            for i in range(length):
                constraint = Constraint()
                constraint.values = [1, 1, 0, 1, 1, 1]
                constraints.append(constraint)
        elif phase_type == 'breaking_contact':
            # set constraints for position control
            for i in range(length):
                constraint = Constraint()
                constraint.values = [1, 1, 1, 1, 1, 1]
                constraints.append(constraint)
        return constraints

    def _get_joint_trajectory(self, data):
        data = data['raw_data']
        trajectory = {}
        trajectory[_JOINT_POSITIONS] = []
        for i in range(data[_JOINT_POSITIONS].shape[1]):
            trajectory[_JOINT_POSITIONS].append(data[_JOINT_POSITIONS][:,i])
        return trajectory

    def _get_dmp_configs(self, phase_params, phase_attrs, phase_types):
        dmp_configs = []
        for phase_key in phase_params.keys():
            elements = phase_params[phase_key]
            for element_key in elements.keys():
                dims = elements[element_key]
                for dim_key in dims.keys():
                    params = dims[dim_key]
                    dmp_config = DMPConfiguration()
                    dmp_config.phase_name = str(phase_key)
                    dmp_config.phase_type = str(phase_types[phase_key])
                    dmp_config.element_name = str(element_key)
                    dmp_config.dimension = str(dim_key)
                    if dim_key == 'rot':
                        for i in range(3):
                            dmp_config.weight_vectors.append(
                                DMPWeightVector(params['w'][i,:].tolist()))
                    else:
                        dmp_config.weight_vectors = [DMPWeightVector(params['w'][:].tolist())]
                    dmp_config.tau = params['tau']
                    dmp_config.alpha = params['alpha']
                    dmp_config.beta = params['beta']
                    dmp_config.gamma = params['gamma']
                    dmp_config.alpha_c = params['alpha_c']
                    dmp_config.alpha_nc = params['alpha_nc']
                    dmp_config.alpha_p = params['alpha_p']
                    dmp_config.alpha_f = params['alpha_f']
                    # need to account for quaternion init/goal for CDMP and floats for normal DMP
                    if dim_key == 'rot':
                        dmp_config.init = params['init']
                        dmp_config.goal = params['goal']
                    else:
                        dmp_config.init = [params['init']]
                        dmp_config.goal = [params['goal']]
                    dmp_config.num_bfs = int(params['num_bfs'])
                    dmp_config.dt = params['dt']
                    # TODO this is super hardcoded to get force goals for making_contact phase
                    if (phase_key == 'phase_1' 
                        and 'force_goal' in phase_attrs['phase_1'].keys()
                        and dim_key in ['0', '1', '2']):
                        dmp_config.force_goal = phase_attrs['phase_1']['force_goal'][int(dim_key)]
                        dmp_config.make_contact_cf = phase_attrs['phase_1']['make_contact_cf']
                        dmp_config.make_contact_mag = phase_attrs['phase_1']['make_contact_mag']
                    dmp_configs.append(dmp_config)
        return dmp_configs
