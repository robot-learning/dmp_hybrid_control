#!/usr/bin/env python
import os
import sys
import errno
import rospy
import numpy as np
from itertools import islice
from pyquaternion import Quaternion as pyQuaternion
from sensor_msgs.msg import JointState
from geometry_msgs.msg import (
    Pose,
    Point,
    Quaternion,
    Twist,
    Wrench,
    Vector3
)

_POSES = 'x'
_TWISTS = 'x_dot'
_ACCELS = 'x_dot_dot'
_WRENCHES = 'w_filt'
_CONSTRAINTS = 'constraints'
_CONSTRAINT_FRAMES = 'constraint_frame'
_NULL_POSTURES = 'null_posture'
_JOINT_POSITIONS = 'q'


class TrajectoryGenerator:

    def __init__(self, path, rate=100):
        self.path = os.path.expanduser(path)
        self.rate = rospy.Rate(rate)
        self.waypoints = []
        self.null_waypoints = []
        self.joint_waypoints = []
        self.trajectory = {}

    def generate_trajectory(self, n_pts=500):
        if len(self.waypoints) < 2:
            rospy.logerr("Trajectory must have at least 2 waypoints. This has %d."
                         % len(self.waypoints))
            return
        
        rospy.loginfo("Generating trajectory. Interpolating waypoints...")
        for i in range(len(self.waypoints) - 1):
            self._interpolate_waypoints(self.waypoints[i], self.waypoints[i+1], n_pts)

        if self.null_waypoints:
            qs = []
            for i in range(len(self.null_waypoints) - 1):
                temp = self._linear_interpolate(self.null_waypoints[i],
                                                self.null_waypoints[i+1], n_pts)
                for q in temp:
                    qs.append(q)
            self.trajectory[_NULL_POSTURES] = qs
            
        rospy.loginfo("Generated trajectory with %d data points." % len(self.trajectory[_POSES]))
        return self.trajectory

    def generate_joint_trajectory(self, n_pts=500):
        if len(self.joint_waypoints) < 2:
            rospy.logerr("Trajectory must have at least 2 waypoints. This has %d."
                         % len(self.waypoints))
            return
        rospy.loginfo("Generating joint trajectory. Interpolating waypoints...")
        for i in range(len(self.joint_waypoints) - 1):
            self._interpolate_joint_waypoints(self.joint_waypoints[i],
                                              self.joint_waypoints[i+1], n_pts)
        rospy.loginfo("Generated joint trajectory with %d data points."
                      % len(self.trajectory[_JOINT_POSITIONS]))
            
    def load_waypoints(self, load_file):
        load_file = os.path.join(self.path, load_file)
        rospy.loginfo("Loading waypoints from %s" % load_file)
        with open(load_file, 'rb') as f:
            while True:
                next_5_lines = list(islice(f, 5))
                if not next_5_lines:
                    break
                pose = [float(p) for p in next_5_lines[0].split()]
                twist = [float(t) for t in next_5_lines[1].split()]
                wrench = [float(w) for w in next_5_lines[2].split()]
                constraint = [int(c) for c in next_5_lines[3].split()]
                waypoint = Waypoint(pose, twist, wrench, constraint)
                self.waypoints.append(waypoint)
        rospy.loginfo("Loaded %d waypoints." % len(self.waypoints))
        
    def load_joint_waypoints(self, load_file):
        load_file = os.path.join(self.path, load_file)
        rospy.loginfo("Loading joint space waypoints from %s" % load_file)
        with open(load_file, 'rb') as f:
            for line in f:
                joint_waypoint = [float(j) for j in line.split()]
                self.joint_waypoints.append(joint_waypoint)
        rospy.loginfo("Loaded %d waypoints." % len(self.joint_waypoints))

    def load_nullspace_waypoints(self, load_file):
        load_file = os.path.join(self.path, load_file)
        rospy.loginfo("Loading nullspace waypoints from %s" % load_file)
        with open(load_file, 'rb') as f:
            for line in f:
                null_posture = [float(j) for j in line.split()]
                self.null_waypoints.append(null_posture)
        rospy.loginfo("Loaded %d nullspace waypoints." % len(self.null_waypoints))
                
    def _interpolate_waypoints(self, wp1, wp2, n_pts=100):
        self.trajectory[_POSES] = []
        self.trajectory[_TWISTS] = []
        self.trajectory[_WRENCHES] = []
        self.trajectory[_NULL_POSTURES] = []
        self.trajectory[_CONSTRAINTS] = []
        # interpolate positions
        ps = self._linear_interpolate(wp1.pose[:3], wp2.pose[:3], n_pts)
        
        # interpolate quaternions
        q1 = pyQuaternion(x=wp1.pose[3], y=wp1.pose[4], z=wp1.pose[5], w=wp1.pose[6])
        q2 = pyQuaternion(x=wp2.pose[3], y=wp2.pose[4], z=wp2.pose[5], w=wp2.pose[6])
        if q1 == q2:  # don't need to interpolate if they're the same
            qs = [q1 for _ in range(n_pts)]
        else:
            qs = pyQuaternion.intermediates(q1, q2, n_pts, include_endpoints=True)
            
        for p, q in zip(ps, qs):
            pose = Pose(Point(*p),
                        Quaternion(q.elements[1], q.elements[2], q.elements[3], q.elements[0]))
            self.trajectory[_POSES].append(pose)  
        # interpolate twists
        ts = self._linear_interpolate(wp1.twist, wp2.twist, n_pts)
        for t in ts:
            self.trajectory[_TWISTS].append(Twist(Vector3(*t[:3]), Vector3(*t[3:])))
        # interpolate wrenchs
        ws = self._linear_interpolate(wp1.wrench, wp2.wrench, n_pts)
        for w in ws:
            self.trajectory[_WRENCHES].append(Wrench(Vector3(*w[:3]), Vector3(*w[3:])))
        # TODO for now assuming constraints stay the same over all waypoints
        self.trajectory[_CONSTRAINTS] = wp1.constraint

    def _interpolate_joint_waypoints(self, wp1, wp2, n_pts=100):
        self.trajectory[_JOINT_POSITIONS] = []
        qs = self._linear_interpolate(wp1, wp2, n_pts)
        for q in qs:
            self.trajectory[_JOINT_POSITIONS].append(JointState(position=q))

    def _linear_interpolate(self, p1, p2, n_pts=100):
        if len(p1) != len(p2):
            rospy.logerr("Points must be of the same dimension.")
            return
        dims = []
        for a, b in zip(p1, p2):
            dims.append(np.linspace(a, b, n_pts))
        return [pt for pt in zip(*dims)]


class Waypoint:
    def __init__(self, pose=None, twist=None, wrench=None, constraint=None):
        self.pose = pose if pose is not None else [0]*6 + [1]
        self.twist = twist if twist is not None else [0]*6
        self.wrench = wrench if wrench is not None else [0]*6
        self.constraint = constraint if constraint is not None else [0]*6
