#!/usr/bin/env python
import sys
import rospy
from matplotlib import colors
from geometry_msgs.msg import Point, PoseStamped
from visualization_msgs.msg import Marker


class TrajectoryVisualizer:

    def __init__(self, pose_topic, marker_topic='visualization_marker', style='point',
                 size=0.01, color='darkgreen', base_frame='lbr4_0_link', rate=100):
        self.base_frame = base_frame
        self.marker = self._get_marker(style, float(size), color)
        self.pose = None
        self.rate = rospy.Rate(float(rate))
        self.marker_pub = rospy.Publisher(marker_topic, Marker, queue_size=1)
        rospy.Subscriber(pose_topic, PoseStamped, self._pose_cb)

    def publish_marker(self):
        self.marker.points.append(Point(self.pose.position.x,
                                        self.pose.position.y,
                                        self.pose.position.z))
        self.marker_pub.publish(self.marker)

    def run(self):
        if self.marker is None:
            rospy.logerr("[TrajectoryVisualizer] Marker could not be created.")
            return
        rospy.loginfo("[TrajectoryVisualizer] Waiting for pose...")
        while not rospy.is_shutdown() and self.pose is None:
            self.rate.sleep()
        rospy.loginfo("[TrajectoryVisualizer] Pose received.")
        rospy.loginfo("[TrajectoryVisualizer] Publishing markers...")
        while not rospy.is_shutdown():
            self.publish_marker()
            self.rate.sleep()
        rospy.loginfo("[TrajectoryVisualizer] Complete. Exiting.")
        
    def _pose_cb(self, pose_stmp_msg):
        self.pose = pose_stmp_msg.pose

    def _get_marker(self, style='point', size=0.01, color='darkgreen'):
        """
        color can be any name from this page:
        http://matplotlib.org/mpl_examples/color/named_colors.hires.png
        """
        converter = colors.ColorConverter()
        c = converter.to_rgba(colors.cnames[color])
        m = Marker()
        m.header.frame_id = self.base_frame
        m.header.stamp = rospy.Time()
        m.id = 0
        if style == 'point':
            m.type = Marker.POINTS
        elif style == 'line':
            m.type = Marker.LINE_STRIP
        else:
            rospy.logerr("[TrajectoryVisualizer] Unknown marker style: %s" % style)
            return None
        m.points = []
        m.action = Marker.MODIFY
        m.pose.orientation.w = 1.0
        m.scale.x = size
        m.color.r = c[0]
        m.color.g = c[1]
        m.color.b = c[2]
        m.color.a = c[3]
        return m

    
if __name__ == '__main__':
    rospy.init_node('visualize_trajectory')
    tv = TrajectoryVisualizer(*rospy.myargv(argv=sys.argv[1:]))
    try:
        tv.run()
    except rospy.ROSInterruptException:
        pass

