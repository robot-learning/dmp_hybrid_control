#!/usr/bin/env python
import sys
import rospy
from sensor_msgs.msg import JointState
from ll4ma_robot_control.msg import OpSpaceCommand
from trajectory_util import (
    TrajectoryGenerator,
    _TRAJECTORY_DIR,
    _POSES,
    _TWISTS,
    _WRENCHES,
    _CONSTRAINTS,
    _NULL_POSTURES,
    _JOINT_POSITIONS,
    _TRAJECTORY_DIR
)


class WaypointCommander:

    def __init__(self, robot_name, path=_TRAJECTORY_DIR, rate=100):
        self.rate = rospy.Rate(rate)
        self.robot_name = robot_name
        self.generator = TrajectoryGenerator(path=path, rate=rate)
        self.op_cmd_pub = rospy.Publisher("/%s/op_space_cmd" % self.robot_name,
                                          OpSpaceCommand, queue_size=1)
        self.joint_cmd_pub = rospy.Publisher("/%s/joint_des_cmd" % self.robot_name,
                                             JointState, queue_size=1)

    def command_trajectory(self, filename, n_pts=1000):
        self.generator.load_waypoints(filename + '.wpt')
        self.generator.load_nullspace_waypoints(filename + '.null')
        self.generator.generate_trajectory(n_pts)
        if not self.generator.trajectory[_POSES]:
            rospy.logerr("Trajectory must have at least one pose to command")
            return

        rospy.loginfo("Publishing trajectory commands to /%s/op_space_cmd" % self.robot_name)
        op_cmd = OpSpaceCommand()
        trajectory = self.generator.trajectory
        for i, p in enumerate(trajectory[_POSES]):
            op_cmd.pose = p
            if len(trajectory[_TWISTS]) > i:
                op_cmd.twist = trajectory[_TWISTS][i]
            if len(trajectory[_WRENCHES]) > i:
                op_cmd.wrench = trajectory[_WRENCHES][i]
            if len(trajectory[_NULL_POSTURES]) > i:
                op_cmd.null_posture = trajectory[_NULL_POSTURES][i]
            op_cmd.constraints = trajectory[_CONSTRAINTS]
            self.op_cmd_pub.publish(op_cmd)
            self.rate.sleep()

    def command_joint_trajectory(self, filename, n_pts=100):
        self.generator.load_joint_waypoints(filename + '.jnt')
        self.generator.generate_joint_trajectory(n_pts)
        if not self.generator.trajectory[_JOINT_POSITIONS]:
            rospy.logerr("Trajectory must have at least one joint position to command")
            return

        rospy.loginfo("Publishing trajectory commands to /%s/joint_des_cmd" % self.robot_name)
        for js in self.generator.trajectory[_JOINT_POSITIONS]:
            self.joint_cmd_pub.publish(js)
            self.rate.sleep()


if __name__ == '__main__':
    rospy.init_node('trajectory_commander')
    if not sys.argv[1:]:
        rospy.logerr("No waypoint filename specified")
    else:
        commander = WaypointCommander('lbr4') # TODO get name off param server
        commander.command_joint_trajectory(sys.argv[1])
        # commander.command_trajectory(sys.argv[1])

