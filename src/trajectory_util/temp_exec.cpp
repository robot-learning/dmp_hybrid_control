#include <ros/ros.h>
#include <std_srvs/Empty.h>


class TestClass
{
public:
  ros::NodeHandle nh_;
  ros::ServiceServer test_service_;

  TestClass(ros::NodeHandle nh) : nh_(nh)
  {
    test_service_ = nh_.advertiseService("test_service", &TestClass::test_service_function, this);
    
  }

  bool test_service_function(std_srvs::Empty::Request &req, std_srvs::Empty::Response &resp)
  {
    ROS_INFO("YOU CALLED THE TEST SERVICE FUNCTION!!!!!!!!!!!!!!");
    return true;
  }

  void run()
  {
    while (ros::ok())
    {
      ros::spin();
    }
  }
};




int main(int argc, char** argv)
{
  ros::init(argc, argv, "temp_exec");
  ros::NodeHandle nh;
  ROS_INFO("TEMP EXEC RUNNING");
  TestClass tc(nh);
  tc.run();
}
