#!/usr/bin/env python
import os
import rospy
import rospkg
import cv2
import cv_bridge
import baxter_interface
import numpy as np
from std_msgs.msg import Bool
from std_srvs.srv import Empty
from sensor_msgs.msg import Image
from optoforce_etherdaq_driver.srv import ZeroForceSensor

class BaxterButtonListener:

    def __init__(self):
        rospy.loginfo("[BaxterButtonListener] Initializing...")
        self.limb = rospy.get_param("/arm")
        self.robot_name = rospy.get_param("/robot_name", "baxter")
        self.rate = rospy.Rate(rospy.get_param("/%s/run_rate" % self.robot_name, 100))
        self.use_ft_sensor = rospy.get_param("/%s/use_ft_sensor" % self.robot_name)
        self.button_state_pub = rospy.Publisher("/%s/button_state" % self.robot_name, Bool,
                                                queue_size=1)
        self.img_pub = rospy.Publisher("/robot/xdisplay", Image, latch=True, queue_size=1)
        rospack = rospkg.RosPack()
        self.img_path = rospack.get_path("ll4ma_robot_control")
        self.img_path = os.path.join(self.img_path, "imgs")
        self.recording = False
        self.handle_demo = False
        
        # try to initialize the arm
        attempts = 0
        arm_initialized = False
        while not arm_initialized and attempts < 5:
            try:
                self.arm = baxter_interface.Limb(self.limb)
                arm_initialized = True
                rospy.loginfo("[BaxterButtonListener] Arm initialized.")
            except OSError:
                attempts += 1
                rospy.loginfo("[BaxterButtonListener] Arm initialization failed. Trying again.")
        if not arm_initialized:
            rospy.logerr("[BaxterButtonListener] Arm could not be initialized. Exiting.")
            return
        else:
            self.init_joint_angles = self.arm.joint_angles()

        # set head to face in general direction of user
        self.head = baxter_interface.Head()
        # if self.limb == "left":
        #     self.head.set_pan(np.pi/4.0)
        # else:
        #     self.head.set_pan(-np.pi/4.0)

        # circle button for start/stop recording
        self.arm_circle_button = baxter_interface.DigitalIO("%s_button_ok" % self.limb)
        self.arm_circle_button.state_changed.connect(self._arm_circle_button_cb)
        # arm back button for discarding recorded demonstration
        self.back_button = baxter_interface.DigitalIO("%s_button_back" % self.limb)
        self.back_button.state_changed.connect(self._arm_back_button_cb)
        # rear shoulder button for moving arm to init at start of each demo
        self.rear_shoulder_button = baxter_interface.DigitalIO("%s_shoulder_button" % self.limb)
        self.rear_shoulder_button.state_changed.connect(self._rear_shoulder_button_cb)
        # side torso button for setting current arm position as init for subsequent demos
        self.side_torso_button = baxter_interface.DigitalIO("torso_%s_button_ok" % self.limb)
        self.side_torso_button.state_changed.connect(self._side_torso_button_cb)
        
        # rethink button for zero force sensor
        if self.use_ft_sensor:
            self.arm_rethink_button = baxter_interface.DigitalIO("%s_button_show" % self.limb)
            self.arm_rethink_button.state_changed.connect(self._arm_rethink_button_cb)
            rospy.loginfo("[BaxterButtonListener] Waiting for zero force service...")
            rospy.wait_for_service("zero_force_sensor")
            rospy.loginfo("[BaxterButtonListener] Zero force service found!")

        rospy.loginfo("[BaxterButtonListener] Waiting for logging services...")
        rospy.wait_for_service("clear_data")
        rospy.wait_for_service("save_data")
        rospy.loginfo("[BaxterButtonListener] Logging services found!")

        rospy.loginfo("[BaxterButtonListener] Initialization complete.")
        self._send_image("ready")

    def run(self):
        rospy.loginfo("[BaxterButtonListener] Listening for button presses...")
        while not rospy.is_shutdown():
            self.rate.sleep()
        rospy.loginfo("[BaxterButtonListener] Complete. Exiting.")

    def shutdown(self):
        self._send_image("researchsdk")
        self.head.set_pan(0.0)

    def _send_image(self, img_name):
        img_path = os.path.join(self.img_path, "%s.png" % img_name)
        img = cv2.imread(img_path)
        msg = cv_bridge.CvBridge().cv2_to_imgmsg(img, encoding="bgr8")
        self.img_pub.publish(msg)
        rospy.sleep(1.0)

    def _arm_circle_button_cb(self, down_press):
        """
        Circle button sends command to demonstration logger to start/stop logging data and
        to indicate in the affirmative that the demonstration should be saved to disk.
        """
        if down_press:
            if self.recording:
                self.button_state_pub.publish(True) # notify demo logger to stop
                self._send_image("keep")
                self.handle_demo = True
                self.recording = False
            elif self.handle_demo:
                success = False
                try:
                    save_data = rospy.ServiceProxy("save_data", Empty)
                    success = save_data()
                except rospy.ServiceException, e:
                    rospy.logwarn("[BaxterButtonListener] Something bad happened with /save_data "
                                  " service call: %s" % e)
                if success:
                    self._send_image("saved")
                    rospy.sleep(1.0)
                    self._send_image("ready")
                else:
                    self._send_image("error")
                self.handle_demo = False
            else:
                self.button_state_pub.publish(True) # notify button logger to start
                self._send_image("recording")
                self.recording = True

    def _arm_rethink_button_cb(self, down_press):
        """
        Rethink button makes service call to zero out force sensor.
        """
        if down_press:
            success = False
            try:
                zero_force_sensor = rospy.ServiceProxy("zero_force_sensor", ZeroForceSensor)
                success = zero_force_sensor(True)
            except rospy.ServiceException, e:
                rospy.logwarn("[BaxterButtonListener] Something bad happened with /zero_force_sensor "
                              "service call: %s" % e)
            if success:
                self._send_image("forces")
                rospy.sleep(1.0)
                self._send_image("ready")
                rospy.loginfo("[BaxterButtonListener] Force sensor zeroed.")

    def _arm_back_button_cb(self, down_press):
        """
        Back button indicates that recorded demonstration should not be saved to disk.
        """
        if down_press:
            if not self.recording and self.handle_demo:
                success = False
                try:
                    clear_data = rospy.ServiceProxy("clear_data", Empty)
                    success = clear_data()
                except rospy.ServiceException, e:
                    rospy.logwarn("[BaxterButtonListener] Something bad happened with /clear_data "
                                  "service call: %s" % e)
                if success:
                    self._send_image("discarded")
                    rospy.sleep(1.0)
                    self.handle_demo = False
                    self._send_image("ready")
                else:
                    self._send_image("error")

    def _rear_shoulder_button_cb(self, down_press):
        """
        Rear shoulder button commands the arm to move back to the initial position for demonstration.
        """
        if down_press:
            self.arm.move_to_joint_positions(self.init_joint_angles)

    def _side_torso_button_cb(self, down_press):
        """
        Side torso button sets the current position of the arm as the new starting point for subsequent demonstrations.
        """
        if down_press:
            self.init_joint_angles = self.arm.joint_angles()
            self._send_image("init")
            rospy.sleep(1.0)
            self._send_image("ready")
            rospy.loginfo("[BaxterButtonListener] Initial joint angles set to current position.")
            

if __name__ == '__main__':
    rospy.init_node('baxter_button_listener')
    bbl = BaxterButtonListener()
    rospy.on_shutdown(bbl.shutdown)
    bbl.run()
