#!/usr/bin/env python
import os
import sys
import tf
import rospy
import h5py
from geometry_msgs.msg import Pose, PoseStamped, Point, Quaternion


class PosePublisher:

    def __init__(self, pose_pub_topic="/pose", base_frame="base", end_frame="end",
                 rate=100, trajectory="", h5_filename='trajectories.h5',
                 path='~/.ros/ll4ma_robot_control'):
        rospy.loginfo("[PosePublisher] Initializing...")
        self.pose_pub_topic = pose_pub_topic
        self.pose_pub = rospy.Publisher(self.pose_pub_topic, PoseStamped, queue_size=1)
        self.base_frame = base_frame
        self.end_frame = end_frame
        self.trajectory = trajectory
        self.h5_file = None
        if self.trajectory != "":
            success = self._set_h5_path(path)
            if success:
                success = self._set_h5_abspath(h5_filename)
            if success:
                self.h5_file = h5py.File(self.h5_abspath, 'r+')
            else:
                rospy.logerr("[PosePublisher] Could not set H5 file.")
                return
        self.rate = rospy.Rate(float(rate))
        self.pose = None
        self.pose_stmp = PoseStamped()
        self.pose_stmp.header.frame_id = self.base_frame
        self.tf_listener = tf.TransformListener()
        rospy.loginfo("[PosePublisher] Initialization complete.")

    def run(self):
        if self.trajectory == "":
            self._publish_tf_poses()
        else:
            self._publish_traj_poses()
        
    def shutdown(self):
        rospy.loginfo("[PosePublisher] Exiting.")

    def _publish_tf_poses(self):
        rospy.loginfo("[PosePublisher] Waiting for transform...")
        while not rospy.is_shutdown() and self.pose is None:
            self._get_tf_pose()
            self.rate.sleep()
        rospy.loginfo("[PosePublisher] Transform received.")
        rospy.loginfo("[PosePublisher] Publishing pose to %s..." % self.pose_pub_topic)
        while not rospy.is_shutdown():
            self._get_tf_pose()
            self.pose_stmp.pose = self.pose
            self.pose_stmp.header.stamp = rospy.Time.now()
            self.pose_pub.publish(self.pose_stmp)
            self.rate.sleep()

    def _publish_traj_poses(self):
        rospy.loginfo("[PosePublisher] Waiting for H5 file to load...")
        while not rospy.is_shutdown() and self.h5_file is None:
            self.rate.sleep()
        rospy.loginfo("[PosePublisher] File loaded.")
        data = self._get_data(self.trajectory)
        rospy.loginfo("[PosePublisher] Publishing poses...")
        for i in range(data.shape[1]):
            pose = Pose(Point(data[0,i], data[1,i], data[2,i]),
                        Quaternion(data[3,i], data[4,i], data[5,i], data[6,i]))
            self.pose_stmp.pose = pose
            self.pose_stmp.header.stamp = rospy.Time.now()
            self.pose_pub.publish(self.pose_stmp)
            self.rate.sleep()
        rospy.loginfo("[PosePublisher] Complete.")
        
    def _get_tf_pose(self):
        trans = None
        rot = None
        try:
            trans, rot = self.tf_listener.lookupTransform(self.base_frame,
                                                          self.end_frame,
                                                          rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException) as e:
            pass
        if trans is not None and rot is not None:
            self.pose = Pose(Point(*trans), Quaternion(*rot))
            
    def _set_h5_path(self, path):
        self.h5_path = os.path.abspath(os.path.expanduser(path))
        if not os.path.exists(self.h5_path):
            print "Path does not exist: %s" % self.h5_path
            success = False
        else:
            success = True
        return success
        
    def _set_h5_abspath(self, h5_filename):
        self.h5_abspath = os.path.join(self.h5_path, h5_filename)
        if not os.path.exists(self.h5_abspath):
            print "File does not exist: %s" % self.h5_abspath
            success = False
        else:
            success = True
        return success

    def _get_data(self, trajectory):
        try:
            data = self.h5_file[trajectory]['raw_data']['x']
        except (KeyError):
            raise KeyError("Trajectory does not exist.")
        return data


if __name__ == '__main__':
    rospy.init_node('pose_publisher')
    pp = PosePublisher(*rospy.myargv(argv=sys.argv[1:]))
    rospy.on_shutdown(pp.shutdown)
    try:
        pp.run()
    except rospy.ROSInterruptException:
        pass
