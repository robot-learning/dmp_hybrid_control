#!/usr/bin/env python
"""
Records an operational space trajectory based on received joint state
and wrench state from a FT sensor.

"""
import os
import errno
import rospy
import rosbag
import pickle
import numpy as np
from tf import transformations as tf
from copy import copy
from urdf_parser_py.urdf import URDF
from pykdl_utils.kdl_kinematics import KDLKinematics
from geometry_msgs.msg import (
    Pose,
    Point,
    Quaternion,
    Twist,
    Vector3,
    WrenchStamped
)
from sensor_msgs.msg import JointState
from ll4ma_robot_control.srv import SetRecorderStatus


class TrajectoryRecorder:

    def __init__(self, robot_name, filename="recorded.traj", path="~/trajectories", rate=100):
        self.robot_name = robot_name
        self._setup_files(filename, path)
        self.robot_model = URDF.from_parameter_server()
        self.base_link = rospy.get_param("root_link")
        self.end_link = rospy.get_param("tip_link") # TODO these might not be namespaced properly
        self.kdl_kin = KDLKinematics(self.robot_model, self.base_link, self.end_link)
        self.joint_state = None
        self.wrench_state = None
        self.poses = []
        self.twists = []
        self.wrenches = []
        self.record = False
        self.rate = rospy.Rate(rate)
        rospy.on_shutdown(self._save_trajectory)
        rospy.Subscriber("/%s/joint_states" % self.robot_name, JointState, self._joint_state_cb)
        rospy.Subscriber("/%s/ft_sensor" % self.robot_name, WrenchStamped, self._wrench_state_cb)
        rospy.Service("set_recorder_status", SetRecorderStatus, self.set_recording_status)
        
    def run(self):
        rospy.loginfo("Ready to record.")
        while not rospy.is_shutdown():
            if self.record:
                if self.joint_state is None:
                    rospy.logwarn("No joint state has been received yet.")
                    continue

                # add pose
                q = self.joint_state.position
                pose_mat = self.kdl_kin.forward(q)
                quat = tf.quaternion_from_matrix(pose_mat)
                self.poses.append(Pose(Point(*pose_mat[:-1,-1]), Quaternion(*quat)))
                # add twist if available
                if self.joint_state.velocity:
                    J = self.kdl_kin.jacobian(q)
                    qdot = np.array(self.joint_state.velocity).reshape((len(q), 1))
                    xdot = J * qdot
                    self.twists.append(Twist(Vector3(*xdot[:3]), Vector3(*xdot[3:])))
                # add wrench if available
                if self.wrench_state is not None:
                    self.wrenches.append(self.wrench_state)
            self.rate.sleep()
        rospy.loginfo("Finished recording. Exiting.")

    def set_recording_status(self, req):
        """
        Returns true if currently recording, false otherwise.
        """
        if self.record and req.record:
            rospy.loginfo("Already recording.")
        elif not self.record and req.record:
            rospy.loginfo("Recording trajectory...")
            self.record = req.record
        elif self.record and not req.record:
            rospy.loginfo("Recording stopped.")
            self.record = req.record
        else:
            rospy.loginfo("Recording already stopped.")
        return self.record

    def _joint_state_cb(self, joint_state_msg):
        self.joint_state = joint_state_msg

    def _wrench_state_cb(self, wrench_state_msg):
        self.wrench_state = wrench_state_msg.wrench

    def _setup_files(self, filename, path):
        path = os.path.expanduser(path)
        try:
            os.makedirs(path)
        except OSError as exception:
            if exception.errno != errno.EEXIST:
                raise
        self.save_file = os.path.join(path, filename)
      
    def _save_trajectory(self):
        rospy.loginfo("Recording complete.")
        if self.poses:
            rospy.loginfo("Saving to file %s" % self.save_file)
            data = {
                'poses':self.poses,
                'twists':self.twists,
                'wrenches':self.wrenches
            }
        
            with open(self.save_file, 'wb') as f:
                pickle.dump(data, f)
                rospy.loginfo("Saving complete. Exiting.")
        else:
            rospy.loginfo("No data to save. Exiting.")
            
                    
if __name__ == '__main__':
    rospy.init_node('trajectory_recorder')
    recorder = TrajectoryRecorder("lbr4") # TODO get robot name from elsewhere
    recorder.run()
