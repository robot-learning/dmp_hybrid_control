import tf
import rospy
from ll4ma_robot_control.msg import OpSpaceCommand
from geometry_msgs.msg import (
    Pose,
    PoseStamped,
    Point,
    Quaternion,
    Twist,
    Wrench,
    Vector3
)
from trajectory_util import (
    _POSES,
    _TWISTS,
    _WRENCHES,
    _CONSTRAINTS,
    _CONSTRAINT_FRAMES,
    _NULL_POSTURES,
)


class TrajectoryCommander:

    def __init__(self):
        rospy.init_node('trajectory_commander')
        self.robot_name = rospy.get_param("/robot_name", "lbr4")
        self.rate = rospy.Rate(rospy.get_param("/%s/run_rate" % self.robot_name, 100))
        self.op_cmd_pub = rospy.Publisher("/%s/op_space_cmd" % self.robot_name,
                                          OpSpaceCommand, queue_size=1)
        self.use_constraint_frame = rospy.get_param("/use_constraint_frame", False)
        self.tip_link = rospy.get_param("/tip_link", "cube_link")
        self.tf = tf.TransformBroadcaster()

    def command_trajectory(self, data, rviz=False):
        trajectory = self._get_trajectory(data)
        if not trajectory[_POSES]:
            raise ValueError("Trajectory is empty.")
        rospy.loginfo("Executing phase '%s' from trajectory '%s'." % (phase_name, trajectory_name))
        rospy.loginfo("Publishing trajectory commands to /%s/op_space_cmd" % self.robot_name)
        op_cmd = OpSpaceCommand()
        keys = trajectory.keys()
        for i, p in enumerate(trajectory[_POSES]):
            op_cmd.pose = p
            if _TWISTS in keys and len(trajectory[_TWISTS]) > i:
                op_cmd.twist = trajectory[_TWISTS][i]
            if _WRENCHES in keys and len(trajectory[_WRENCHES]) > i:
                op_cmd.wrench = trajectory[_WRENCHES][i]
            if _NULL_POSTURES in keys and len(trajectory[_NULL_POSTURES]) > i:
                op_cmd.null_posture = trajectory[_NULL_POSTURES][i]
            if _CONSTRAINTS in keys and len(trajectory[_CONSTRAINTS]) > i:
                op_cmd.constraints = trajectory[_CONSTRAINTS][i]
            if _CONSTRAINT_FRAMES in keys and len(trajectory[_CONSTRAINT_FRAMES]) > i:
                op_cmd.constraint_frame = trajectory[_CONSTRAINT_FRAMES][i]
                if rviz:
                    pos = op_cmd.constraint_frame.position
                    q = op_cmd.constraint_frame.orientation
                    self.tf.sendTransform((pos.x, pos.y, pos.z),
                                          (q.x, q.y, q.z, q.w),
                                          rospy.Time.now(),
                                          "constraint_frame",
                                          "world")
            self.op_cmd_pub.publish(op_cmd)
            self.rate.sleep()

    def _get_trajectory_data(self, data):
        trajectory = {}
        trajectory[_POSES] = []
        trajectory[_WRENCHES] = []
        trajectory[_CONSTRAINTS] = []
        trajectory[_CONSTRAINT_FRAMES] = []

        for i in range(data[_POSES].shape[1]):
            p = data[_POSES][:, i]
            trajectory[_POSES].append(Pose(Point(*p[:3]), Quaternion(*p[3:])))
            # if _WRENCHES in data.keys():
            #     w = data[_WRENCHES][:, i]
            #     trajectory[_WRENCHES].append(Wrench(Vector3(*w[:3]), Vector3(*w[3:])))
                

            if _WRENCHES in data.keys() and not self.use_constraint_frame:
                w = data[_WRENCHES][:, i]
                trajectory[_WRENCHES].append(Wrench(Vector3(*w[:3]), Vector3(*w[3:])))
            if _CONSTRAINTS in data.keys():
                trajectory[_CONSTRAINTS].append(data[_CONSTRAINTS][:, i])
            if _CONSTRAINT_FRAMES in data.keys():
                cf = data[_CONSTRAINT_FRAMES][:, i]
                trajectory[_CONSTRAINT_FRAMES].append(Pose(Point(),
                                                           Quaternion(cf[0], cf[1], cf[2], cf[3])))
        # TODO add other components of trajectory
        print "Complete."
        return trajectory
