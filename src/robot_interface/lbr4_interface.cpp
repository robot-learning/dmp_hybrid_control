#include "lbr4_interface.h"


namespace robot_interface
{

  bool LBR4Interface::init(std::vector<std::string> &jnt_names)
  {
    ROS_INFO("[LBR4Interface] Initializing...");

    if (!RobotInterface::init(jnt_names))
    {
      ROS_ERROR("[LBR4Interface] Base interface initialization FAILURE.");
      return false;
    }

    jnt_cmd_pub_ = nh_.advertise<sensor_msgs::JointState>(jnt_cmd_topic_, 1);

    this->initJointState(jnt_cmd_);

    ROS_INFO("[LBR4Interface] Initialization complete.");
    return true;
  }
  
  void LBR4Interface::publishTorqueCommand(Eigen::VectorXd &torques)
  {
    this->saturateTorques(torques);
    for (int i = 0; i < num_jnts_; i++)
      jnt_cmd_.effort[i] = torques[i];
    jnt_cmd_pub_.publish(jnt_cmd_);
  }

  void LBR4Interface::initJointState(sensor_msgs::JointState &jnt_state)
  {
    ROS_INFO("[LBR4Interface] Initializing joint state...");
    jnt_state = sensor_msgs::JointState();
    for (int i=0; i < num_jnts_; ++i)
    {
      jnt_state.name.push_back(jnt_names_[i]);
      jnt_state.position.push_back(0.0);
      jnt_state.velocity.push_back(0.0);
      jnt_state.effort.push_back(0.0);
    }
    ROS_INFO("[LBR4Interface] Joint state initialization complete.");
  }

  void LBR4Interface::compensateGravity(Eigen::VectorXd &tau, Eigen::VectorXd &gravity)
  {
    tau += gravity;
  }


}
