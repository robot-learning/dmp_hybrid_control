#include "robot_interface.h"


namespace robot_interface
{

  bool RobotInterface::init(std::vector<std::string> &jnt_names)
  {
    ROS_INFO("[RobotInterface] Initializing...");
    jnt_names_ = jnt_names;
    num_jnts_ = jnt_names_.size();

    if (!nh_.getParam("torque_limits", torque_lims_))
    {
      ROS_ERROR("[RobotInterface] Initialization FAILURE. Torque limits not on parameter server.");
      return false;
    }
    if (!nh_.getParam("jnt_cmd_topic", jnt_cmd_topic_))
    {
      ROS_ERROR("[RobotInterface] Initialization FAILURE. Joint command topic not on parameter server.");
      return false;
    }
    
    ROS_INFO("[RobotInterface] Initialization complete.");
    return true;
  }
  
  void RobotInterface::saturateTorques(Eigen::VectorXd &torques)
  {
    for (int i = 0; i < num_jnts_; i++)
      if (std::abs(torques[i]) > torque_lims_[i])
	torques[i] = copysign(torque_lims_[i], torques[i]);
  }

  void RobotInterface::publishTorqueCommand(Eigen::VectorXd &torques)
  {
    // intentionally left empty, this class should be extended
  }

  void RobotInterface::compensateGravity(Eigen::VectorXd &tau, Eigen::VectorXd &gravity)
  {
    // intentionally left empty, this class should be extended
  }

  void RobotInterface::getSEAJointState(Eigen::VectorXd &spring, Eigen::VectorXd &cross, 
					Eigen::VectorXd &gravity, Eigen::VectorXd &gravity_model)
  {
    // intentionally left empty, TEMPORARY FUNCTION ON THIS
  }


}
