#!/usr/bin/env python
import sys
import rospy
import numpy as np
from copy import copy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import WrenchStamped, Vector3


class ForceFilter:

    def __init__(self, order=1, look_ahead=50, rate=1000):
        try:
            self.robot_name = rospy.get_param("robot_name")
            self.ft_topic = rospy.get_param("/%s/ft_topic" % self.robot_name)
            self.filtered_topic = rospy.get_param("/%s/filtered_ft_topic" % self.robot_name)
            self.ft_link = rospy.get_param("/%s/ft_link" % self.robot_name)
        except (KeyError):
            rospy.logerr("[ForceFilter] Parameters not found on param server.")
            return
            
        rospy.Subscriber(self.ft_topic, WrenchStamped, self._raw_force_cb)
        self.filtered_pub = rospy.Publisher(self.filtered_topic, WrenchStamped, queue_size=1)
        self.filtered = WrenchStamped()
        self.filtered.header.frame_id = self.ft_link
        self.order = order
        self.look_ahead = look_ahead
        self.window = None
        self.rate = rospy.Rate(rate)
    
    def run(self):
        rospy.loginfo("[ForceFilter] Waiting for %d raw force samples to start filtering..."
                      % (2*self.look_ahead))
        while not rospy.is_shutdown() and self.window is None:
            self.rate.sleep()
        while not rospy.is_shutdown() and self.window.shape[1] < 2*self.look_ahead+1:
            self.rate.sleep()
        rospy.loginfo("[ForceFilter] Samples received. Filtering forces...")
        while not rospy.is_shutdown():
            smoothed = self._filter_force()
            self.filtered.header.stamp = rospy.Time.now()
            self.filtered.wrench.force = Vector3(*smoothed[:3])
            self.filtered.wrench.torque = Vector3(*smoothed[3:])
            self.filtered_pub.publish(self.filtered)
            self.rate.sleep()
    
    def shutdown(self):
        rospy.loginfo("[ForceFilter] Exiting.")

    def _filter_force(self):
        xs = np.arange(-self.look_ahead, self.look_ahead+1)
        M = np.cumprod(np.hstack((np.ones(2*self.look_ahead+1)[:,None],
                                  np.tile(xs, (self.order, 1)).T)), axis=1)
        df, _, _, _ = np.linalg.lstsq(M, np.eye(2*self.look_ahead+1))
        hx = np.array([self.look_ahead**i for i in range(self.order+1)])
        # SG filter coefficients
        fc = np.dot(df.T, hx.T)
        # smoothing
        smoothed = np.zeros(6)
        for i in range(6):
            data = self.window[i,:]
            smoothed[i] = np.dot(fc, data)
        return np.array(smoothed)

    def _raw_force_cb(self, wrench_stmp):
        w = wrench_stmp.wrench
        current = np.array([w.force.x, w.force.y, w.force.z,
                            w.torque.x, w.torque.y, w.torque.z])
        if self.window is None:
            self.window = current.reshape((6,1))
        elif self.window.shape[1] < 2*self.look_ahead+1:
            self.window = np.hstack((self.window, current.reshape((6,1))))
        else:
            self.window = np.roll(self.window, -1)
            self.window[:,-1] = current        
            

if __name__ == '__main__':
    rospy.init_node('force_filter')
    ff = ForceFilter()
    rospy.on_shutdown(ff.shutdown)
    try:
        ff.run()
    except rospy.ROSInterruptException:
        pass

